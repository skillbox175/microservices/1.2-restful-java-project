public class Libs {

    public static final String LIQUIBASE = "org.liquibase:liquibase-core:" + LibVers.LIQUIBASE_VERSION;
    public static final String VAVR = "io.vavr:vavr:" + LibVers.VAVR_VERSION;
    public static final String SWAGGER = "org.springdoc:springdoc-openapi-ui:" + LibVers.SWAGGER_VERSION;
    public static final String JACKSON_JAVA = "com.fasterxml.jackson.core:jackson-databind:" + LibVers.JACKSON_VERSION;
    public static final String JACKSON_DATATYPE = "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:" + LibVers.JACKSON_VERSION;
    public static final String JACOCO_REPORT = "org.jacoco:org.jacoco.report:" + LibVers.JACOCO_REPORT_VERSION;
    public static final String KAFKA = "org.springframework.kafka:spring-kafka";
    public static final String POSTGRESQL = "org.postgresql:postgresql";
    public static final String SNAKE_YAML = "org.yaml:snakeyaml:" + LibVers.SNAKE_YAML_VERSION;
    public static final String HATEOAS = "org.springframework.boot:spring-boot-starter-hateoas:" + LibVers.HATEOAS_VERSION;

    //testcontainers
    public static final String TESTCONTAINERS_BOM = "org.testcontainers:testcontainers-bom:" + LibVers.TESTCONTAINERS_BOM_VERSION;
    public static final String TESTCONTAINERS_POSTGRESQL = "org.testcontainers:postgresql";
    public static final String TESTCONTAINERS_KAFKA = "org.testcontainers:kafka";


    public static final String WIREMOCK = "org.springframework.cloud:spring-cloud-contract-wiremock:" + LibVers.WIREMOCK_VERSION;

    //lombok
    public static final String LOMBOK = "org.projectlombok:lombok";

    //junit
    public static final String JUNIT = "org.junit.jupiter:junit-jupiter-engine:" + LibVers.JUNIT_VERSION;
    public static final String JUNIT_API = "org.junit.jupiter:junit-jupiter-api:" + LibVers.JUNIT_VERSION;

    //tests
    public static final String JAVA_FAKER = "com.github.javafaker:javafaker:" + LibVers.JAVA_FAKER_VERSION;
}
