abstract class Global {
    static final String JAVA_VERSION = "17.0.2";

    static final String SPRING_CLOUD_VERSION = "2021.0.3";

    static final String SPRING_BOOT_VERSION = "2.7.8";
}
