public class LibVers {
    public static final String SWAGGER_VERSION = "1.6.9";
    public static final String VAVR_VERSION = "0.10.4";
    public static final String JACKSON_VERSION = "2.15.2";
    public static final String LIQUIBASE_VERSION = "4.29.0";
    public static final String JACOCO_REPORT_VERSION = "0.8.8";
    public static final String TESTCONTAINERS_BOM_VERSION = "1.20.1";
    public static final String WIREMOCK_VERSION = "2.1.3.RELEASE";
    public static final String JUNIT_VERSION = "5.9.3";
    public static final String SNAKE_YAML_VERSION = "1.33";
    public static final String HATEOAS_VERSION = "3.1.2";
    public static final String JAVA_FAKER_VERSION = "1.0.2";
}
