package ru.skillbox.microservices.user.common.events;

import ru.skillbox.microservices.user.common.types.base.DomainEvent;

/**
 * This interface defines a method for handling a DomainEvent.
 *
 * @param <T> The type of the DomainEvent to be handled.
 */
public interface DomainEventListener<T extends DomainEvent> {
    /**
     * Returns the type of the event that this DomainEventListener is listening for.
     *
     * @return the class of the event type
     */
    Class<?> eventType();

    /**
     * Handles the given DomainEvent.
     *
     * @param  event  the DomainEvent to be handled
     */
    void handle(DomainEvent event);
}
