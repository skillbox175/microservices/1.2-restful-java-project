package ru.skillbox.microservices.user.common.events;

import ru.skillbox.microservices.user.common.types.base.DomainEvent;

import java.util.Collection;

public interface DomainEventPublisher {

    /**
     * Publishes a collection of domain events.
     *
     * @param  events  the collection of domain events to publish
     */
    void publish(Collection<? extends DomainEvent> events);
}
