package ru.skillbox.microservices.user.common.types.base;

import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class EventIdTest {
    @Test
    void create_event__check_event_id_is_unique() {
        var firstEvent = new EmptyEvent();
        var secondEvent = new EmptyEvent();

        assertNotEquals(firstEvent.getId(), secondEvent.getId());
        assertNotEquals(firstEvent.getId().getValue(), secondEvent.getId().getValue());
    }

    @NoArgsConstructor
    private class EmptyEvent extends DomainEvent{

    }
}
