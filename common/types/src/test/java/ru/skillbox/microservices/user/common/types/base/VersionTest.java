package ru.skillbox.microservices.user.common.types.base;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VersionTest {
    @Test
    void newId_CheckVersionIsZero() {
        Version firstVersion = new Version();
        Version secondVersion = new Version();

        assertEquals(0, firstVersion.toLongValue());
        assertEquals(firstVersion.toLongValue(), secondVersion.toLongValue());
    }

    @Test
    void restoreFromLong() {
        Random random = new Random();
        Long value = random.nextLong();
        Version version = Version.fromLong(value);

        assertEquals(version.toLongValue(), value);
    }

    @Test
    void incrementCounter() {
        Random random = new Random();
        long value = random.nextLong();
        Version version = Version.fromLong(value);
        Version incremented = version.next();
        assertEquals(incremented.toLongValue(), value + 1);
    }

    @Test
    void decrementCounter() {
        Random random = new Random();
        long value = random.nextLong();
        Version version = Version.fromLong(value);
        Version decremented = version.previous();
        assertEquals(decremented.toLongValue(), value - 1);
    }
}
