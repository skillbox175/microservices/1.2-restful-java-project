package ru.skillbox.microservices.user.common.types.base;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@SuperBuilder(toBuilder = true)
@EqualsAndHashCode
@ToString
public abstract class DomainEntity<T> {
    public final T id;
    @Builder.Default
    public Version version = new Version();
    @Builder.Default
    private List<DomainEvent> events = new ArrayList<>();

    /**
     * Adds a new event to the list of events.
     *
     * @param  event the event to be added
     */
    public void addEvent(DomainEvent event) {
        if (events.isEmpty()) {
            version = version.next();
        }
        events.add(event);
    }

    /**
     * Retrieves and removes all pending events associated with this entity.
     *
     * @return         	the list of pending domain events
     */
    public List<DomainEvent> popEvents() {
        var result = events;
        events = new ArrayList<>();
        return result;
    }

}
