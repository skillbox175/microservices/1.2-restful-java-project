package ru.skillbox.microservices.user.common.types.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.UUID;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EventId implements ValueObject {
    UUID value;

    public static EventId generate() {
        return new EventId(UUID.randomUUID());
    }

}
