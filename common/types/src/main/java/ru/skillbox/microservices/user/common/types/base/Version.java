package ru.skillbox.microservices.user.common.types.base;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.*;

@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Value
@ToString
public class Version implements ValueObject{
    Long value;

    public Version() {
        this.value = 0L;
    }

    /**
     * Creates a new Version object from a given Long value.
     *
     * @param  value  the Long value to create the Version object from
     * @return        a new Version object with the given Long value
     */
    public static Version fromLong(Long value) {
        return new Version(value);
    }

    /**
     * Returns the Long value of the Version object.
     *
     * @return the Long value of the Version object
     */
    @JsonGetter("value")
    public Long toLongValue() {
        return value;
    }

    /**
     * Returns the next Version object by incrementing the Long value by 1.
     *
     * @return         a new Version object with the Long value incremented by 1
     */
    public Version next() {
        return new Version(value + 1);
    }

    /**
     * Returns a new Version object that is one less than the current Version.
     *
     * @return a new Version object with the value decremented by one
     */
    public Version previous() {
        return new Version(value - 1);
    }
}
