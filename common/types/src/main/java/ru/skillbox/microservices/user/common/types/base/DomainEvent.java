package ru.skillbox.microservices.user.common.types.base;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.time.OffsetDateTime;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class DomainEvent {
    private final EventId id = EventId.generate();
    private final Version version = new Version();
    private final OffsetDateTime created = OffsetDateTime.now();
}