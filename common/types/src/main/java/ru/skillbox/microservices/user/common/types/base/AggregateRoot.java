package ru.skillbox.microservices.user.common.types.base;

import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public abstract class AggregateRoot<T> extends DomainEntity<T> {
}