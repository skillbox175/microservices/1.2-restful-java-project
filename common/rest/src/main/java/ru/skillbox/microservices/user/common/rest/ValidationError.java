package ru.skillbox.microservices.user.common.rest;

public record ValidationError(String message) {
}
