package ru.skillbox.microservices.user.common.rest;

import lombok.SneakyThrows;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

public class RestResponses {

    private RestResponses() {
    }

    private static final String BASE_URL = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();

    @SneakyThrows
    public static ResponseEntity<Problem> restBusinessError(String title, String code) {
        return ResponseEntity
                .unprocessableEntity()
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(
                        Problem.create().withStatus(HttpStatus.UNPROCESSABLE_ENTITY)
                                .withType(new URI(String.format("%s/%s", BASE_URL, code)))
                                .withTitle(title)
                );
    }

    @SneakyThrows
    public static ResponseEntity<Problem> resourceNotFound() {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(
                        Problem.create().withStatus(HttpStatus.NOT_FOUND)
                                .withType(new URI(String.format("%s/resource_not_found", BASE_URL)))
                                .withTitle("Resource not found")
                );
    }

    @SneakyThrows
    public static ResponseEntity<Problem.ExtendedProblem<Map<String, List<ValidationError>>>>
    toInvalidParamsBadRequest(List<ValidationError> errorList) {
        return ResponseEntity
                .badRequest()
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(
                        Problem.create(Map.of("invalid_params", errorList))
                                .withStatus(HttpStatus.BAD_REQUEST)
                                .withType((new URI(BASE_URL + "/bad_request")))
                                .withTitle("Bad request")
                );
    }

    @SneakyThrows
    public static ResponseEntity<String> created(URI location) {
        return ResponseEntity.created(location).build();
    }

    @SneakyThrows
    public static ResponseEntity<String> noContent() {
        return ResponseEntity.noContent().build();
    }
}
