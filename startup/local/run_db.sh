docker run --rm --name user-service-backend-db-local \
        -e POSTGRES_DB=user-service-backend-db \
        -e POSTGRES_USER=root \
        -e POSTGRES_PASSWORD=123 \
        -p 5001:5432 \
        -d postgres:15.1-alpine