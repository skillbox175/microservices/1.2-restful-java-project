#!/usr/bin/env bash
set -e
echo "Starting postgres container"
docker compose up --force-recreate --remove-orphans -d user-service-database --env-file ./startup/compose/stage.env