insert into city (city)
values ('Moscow'),
       ('Yekaterinburg'),
       ('Voronezh');

insert into gender (gender)
values ('male'),
       ('female');



insert into users (first_name, last_name, patronymic_name, gender_id, city_id)
values ('Vasya', 'Ivanov', 'Petrovich', '1', '1'),
       ('Petya', 'Sidorov', 'Ivanovich', '1', '2'),
       ('Vanya', 'Petrov', 'Sidorovich', '1', '3');

insert into user_subscription (channel_id, subscriber_id)
values (1, 2),
       (2, 1),
       (1, 3),
       (2, 3);
