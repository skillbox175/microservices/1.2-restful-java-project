create index i_users_city
    on users using HASH (city_id);

create index i_users_gender
    on users using HASH (gender_id);
