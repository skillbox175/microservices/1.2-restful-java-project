package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import ru.skillbox.microservices.user.common.types.base.ValueObject;
import ru.skillbox.microservices.user.common.types.error.BusinessError;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class UserId implements ValueObject {
    Long value;

    public static Either<CreateUserIdError, UserId> fromLong(Long value) {
        if (value > 0 && value < Long.MAX_VALUE) {
            return Either.right(new UserId(value));
        } else if (value < 0) {
            return Either.left(new NegativeIdValueError());
        } else if (value >= Long.MAX_VALUE) {
            return Either.left(new MaxIdValueExceededError());
        } else {
            return Either.left(new EmptyIdValueError());
        }
    }

    public Long toLongValue() {
        return value;
    }

    public abstract static class CreateUserIdError implements BusinessError {
        private CreateUserIdError() {
        }
    }

    public static final class NegativeIdValueError extends CreateUserIdError {
    }

    public static final class EmptyIdValueError extends CreateUserIdError {
    }

    public static final class MaxIdValueExceededError extends CreateUserIdError {
    }
}

