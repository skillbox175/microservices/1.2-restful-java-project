package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import ru.skillbox.microservices.user.common.types.base.ValueObject;
import ru.skillbox.microservices.user.common.types.error.BusinessError;

import java.util.Objects;
import java.util.regex.Pattern;


@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class UserName implements ValueObject {
    private static final String USER_NAME_FORMAT = "^[A-Z][a-z]+$";
    String value;

    public static Either<CreateUserNameError, UserName> fromString(String value) {
        if (Objects.equals(value, "") && Objects.equals(value, " ")) {
            return Either.left(new EmptyUserNameError());
        } else if (!checkFormat(value)) {
            return Either.left(new WrongUserNameFormatError());
        } else {
            return Either.right(new UserName(value));
        }
    }

    public String toStringValue() {
        return value;
    }

    public static boolean checkFormat(String value) {
        return Pattern.matches(USER_NAME_FORMAT, value);
    }


    public abstract static class CreateUserNameError implements BusinessError {

        private CreateUserNameError() {
        }

    }

    public static final class EmptyUserNameError extends CreateUserNameError {
    }

    public static final class WrongUserNameFormatError extends CreateUserNameError {
    }
}
