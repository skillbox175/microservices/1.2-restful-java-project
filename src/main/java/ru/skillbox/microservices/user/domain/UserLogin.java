package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import ru.skillbox.microservices.user.common.types.base.ValueObject;
import ru.skillbox.microservices.user.common.types.error.BusinessError;

import java.util.Objects;
import java.util.regex.Pattern;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class UserLogin implements ValueObject {
    private static final String USER_LOGIN_FORMAT = "^[a-z]+\\.[a-z]+$";
    String value;

    public static Either<UserLogin.CreateUserLoginError, UserLogin> fromString(String value) {
        if (Objects.equals(value, "") && Objects.equals(value, " ")) {
            return Either.left(new UserLogin.EmptyUserLoginError());
        } else if (!checkFormat(value)) {
            return Either.left(new UserLogin.WrongUserLoginFormatError());
        } else {
            return Either.right(new UserLogin(value));
        }
    }

    public String toStringValue() {
        return value;
    }

    public static boolean checkFormat(String value) {
        return Pattern.matches(USER_LOGIN_FORMAT, value);
    }

    public abstract static class CreateUserLoginError implements BusinessError {

        private CreateUserLoginError() {
        }
    }

    public static final class EmptyUserLoginError extends CreateUserLoginError {
    }

    public static final class WrongUserLoginFormatError extends CreateUserLoginError {
    }
}

