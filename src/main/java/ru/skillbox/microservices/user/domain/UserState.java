package ru.skillbox.microservices.user.domain;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import ru.skillbox.microservices.user.common.types.base.ValueObject;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class UserState implements ValueObject {
    Long id;
    String userLogin;
    String firstName;
    String lastName;
    String patronymicName;
    boolean deleted;
    String photoId;

    public static UserState fromUserEntity(UserEntity user) {
        return new UserState(
                user.id.toLongValue(),
                user.userLogin.toStringValue(),
                user.firstName.toStringValue(),
                user.lastName.toStringValue(),
                user.patronymicName.toStringValue(),
                user.deleted,
                user.photoId.map(PhotoId::toStringValue).orElse("")
        );
    }
}
