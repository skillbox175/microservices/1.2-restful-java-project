package ru.skillbox.microservices.user.domain.events;


import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserAddedDomainEvent extends DomainEvent {
    private final UserId userId;
    private final UserEntity payload;
}
