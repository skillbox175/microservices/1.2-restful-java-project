package ru.skillbox.microservices.user.domain.events;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;
import ru.skillbox.microservices.user.domain.UserId;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserRemovedDomainEvent extends DomainEvent {
    private final UserId userId;
}
