package ru.skillbox.microservices.user.domain;

import ru.skillbox.microservices.user.common.types.base.Version;

import java.util.Optional;

public class UserRestorer {

    private UserRestorer() {

    }
    public static UserEntity restoreUser(
            UserId id,
            UserLogin login,
            UserName firstName,
            UserName patronymicName,
            UserName lastName,
            boolean deleted,
            Version version,
            Optional<PhotoId> photoId
    ) {
        return UserEntity.builder()
                .id(id).userLogin(login)
                .firstName(firstName)
                .lastName(lastName)
                .patronymicName(patronymicName)
                .deleted(deleted)
                .version(version)
                .photoId(photoId != null ? photoId : Optional.empty())
                .build();
    }

    public static UserEntity restoreUser(
            UserEntity user
    ) {
        return UserEntity.builder()
                .id(user.id).userLogin(user.userLogin)
                .firstName(user.firstName)
                .lastName(user.lastName)
                .patronymicName(user.patronymicName)
                .deleted(user.deleted)
                .version(user.version)
                .build();
    }


}
