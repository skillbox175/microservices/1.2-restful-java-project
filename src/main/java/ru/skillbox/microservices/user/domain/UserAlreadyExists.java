package ru.skillbox.microservices.user.domain;

public interface UserAlreadyExists {
    boolean invoke(UserLogin userLogin);

    boolean invoke(UserId userId);
}
