package ru.skillbox.microservices.user.domain;

public interface UserAlreadyFollowing {
    boolean invoke(UserId followingId, UserId currUserId);
}
