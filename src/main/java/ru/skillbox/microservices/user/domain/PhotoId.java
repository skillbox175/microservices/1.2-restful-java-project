package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import ru.skillbox.microservices.user.common.types.base.ValueObject;
import ru.skillbox.microservices.user.common.types.error.BusinessError;

import java.util.UUID;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PhotoId implements ValueObject {
    UUID value;

    public static Either<CreatePhotoIdError, PhotoId> fromString(String value) {
        if (value.isBlank()) {
            return Either.left(new EmptyStringValue());
        }

        try {
            UUID.fromString(value);
        } catch (IllegalArgumentException e) {
            return Either.left(new WrongStringValue());
        }

        return Either.right(new PhotoId(UUID.fromString(value)));
    }

    public String toStringValue() {
        return value.toString();
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public abstract static class CreatePhotoIdError implements BusinessError {

    }

    public static final class WrongStringValue extends CreatePhotoIdError {

    }

    public static final class EmptyStringValue extends CreatePhotoIdError {

    }

}
