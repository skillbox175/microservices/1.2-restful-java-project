package ru.skillbox.microservices.user.domain.events;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserState;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserUpdatedDomainEvent extends DomainEvent {
    private final UserId userId;
    private final Map<String, UserState> payload;
}
