package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import ru.skillbox.microservices.user.common.types.base.AggregateRoot;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.events.UserAddedDomainEvent;
import ru.skillbox.microservices.user.domain.events.UserRemovedDomainEvent;
import ru.skillbox.microservices.user.domain.events.UserUpdatedDomainEvent;

import java.util.HashMap;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class UserEntity extends AggregateRoot<UserId> {
    public final UserLogin userLogin;
    public final UserName firstName;
    public final UserName lastName;
    public final UserName patronymicName;
    @Getter
    @Builder.Default
    public boolean deleted = false;
    @Getter
    @Builder.Default
    public Optional<PhotoId> photoId = Optional.empty();

    static final String PREVIOUS_USER_STATE = "previousUserState";
    static final String UPDATED_USER_STATE = "updatedUserState";

    public static Either<UserEntityError, UserEntity> addUser(
            UserIdGenerator userIdGenerator,
            UserAlreadyExists userAlreadyExist,
            UserLogin userLogin,
            UserName firstName,
            UserName patronymicName,
            UserName lastName) {

        if (userAlreadyExist.invoke(userLogin)) {
            return Either.left(new AlreadyExistsWithSameLoginError());
        } else {
            UserId userId = userIdGenerator.generate();
            var user = UserEntity.builder()
                    .id(userId)
                    .userLogin(userLogin)
                    .firstName(firstName)
                    .lastName(lastName)
                    .patronymicName(patronymicName)
                    .build();
            user.addEvent(new UserAddedDomainEvent(userId, user));
            return Either.right(user);
        }
    }

    public Either<UserEntityError, UserEntity> updateUser(
            UserLogin userLogin,
            UserName firstName,
            UserName patronymicName,
            UserName lastName
    ) {
            var updatedUser = UserEntity.builder()
                    .id(super.id)
                    .userLogin(userLogin)
                    .firstName(firstName)
                    .lastName(lastName)
                    .patronymicName(patronymicName)
                    .version(super.version)
                    .build();
        var payload = new HashMap<String, UserState>();
        payload.put(PREVIOUS_USER_STATE, UserState.fromUserEntity(this));
        payload.put(UPDATED_USER_STATE, UserState.fromUserEntity(updatedUser));
        updatedUser.addEvent(new UserUpdatedDomainEvent(super.id, payload));
        return Either.right(updatedUser);

    }

    public void removeUser() {
        if (!deleted) {
            deleted = true;
            addEvent(new UserRemovedDomainEvent(super.id));
        }
    }

    public Either<UserEntityError, UserEntity> setPhoto(PhotoId photoId) {
        var currentUserState = UserEntity.builder()
                .id(super.id)
                .userLogin(userLogin)
                .firstName(firstName)
                .lastName(lastName)
                .patronymicName(patronymicName)
                .deleted(deleted)
                .photoId(Optional.of(photoId))
                .version(super.version)
                .build();
        var payload = new HashMap<String, UserState>();
        payload.put(PREVIOUS_USER_STATE, UserState.fromUserEntity(this));
        payload.put(UPDATED_USER_STATE, UserState.fromUserEntity(currentUserState));
        this.photoId = Optional.of(photoId);
        this.addEvent(new UserUpdatedDomainEvent(super.id, payload));

        return Either.right(this);
    }

    public Either<UserEntityError, UserEntity> removePhoto() {
        if (photoId.isEmpty()) {
            return Either.right(this);
        }
        var currentUserState = UserEntity.builder()
                .id(super.id)
                .userLogin(userLogin)
                .firstName(firstName)
                .lastName(lastName)
                .patronymicName(patronymicName)
                .deleted(deleted)
                .version(super.version)
                .build();
        var payload = new HashMap<String, UserState>();
        payload.put(PREVIOUS_USER_STATE, UserState.fromUserEntity(this));
        payload.put(UPDATED_USER_STATE, UserState.fromUserEntity(currentUserState));
        currentUserState.addEvent(new UserUpdatedDomainEvent(super.id, payload));
        return Either.right(currentUserState);
    }

    public boolean visible() {
        return !deleted;
    }

    public abstract static class UserEntityError implements BusinessError {
        protected UserEntityError() {
        }
    }

    public static class AlreadyExistsWithSameLoginError extends UserEntityError {
    }

    public static class NotFoundError extends UserEntityError {
    }

    public static class NotFollowingError extends UserEntityError {
    }

    public static class AlreadyFollowingError extends UserEntityError {
    }

}