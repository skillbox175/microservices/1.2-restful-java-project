package ru.skillbox.microservices.user.domain;

@FunctionalInterface
public interface UserIdGenerator {
    UserId generate();
}
