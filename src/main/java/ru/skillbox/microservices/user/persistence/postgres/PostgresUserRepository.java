package ru.skillbox.microservices.user.persistence.postgres;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.skillbox.microservices.user.common.events.DomainEventPublisher;
import ru.skillbox.microservices.user.domain.PhotoId;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.events.UserAddedDomainEvent;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.access.UserPersister;

import javax.sql.DataSource;
import java.util.*;

public class PostgresUserRepository implements UserPersister, UserExtractor {

    public final DataSource dataSource;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DomainEventPublisher eventPublisher;

    public PostgresUserRepository(DataSource dataSource, DomainEventPublisher eventPublisher) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.eventPublisher = eventPublisher;
    }

    @Override
    public Optional<UserEntity> getById(UserId id) {
        var params = Map.of("id", id.toLongValue());
        return jdbcTemplate.query("SELECT * from public.users WHERE id = :id", params, new UserResultSetExtractor());
    }

    @Override
    public Optional<UserEntity> getByLogin(UserLogin login) {
        var params = Map.of("login", login.toStringValue());
        return jdbcTemplate.query("SELECT * from public.users WHERE login = :login", params, new UserResultSetExtractor());
    }

    @Override
    public List<UserEntity> getAll() {

        return jdbcTemplate.query("SELECT * FROM public.users WHERE NOT deleted", new UserRowMapper());
    }

    @Override
    public Set<UserEntity> getFollowingById(UserId followerId) {
        var params = Map.of("id", followerId.toLongValue());
        var followingIdList = jdbcTemplate.query(
                "SELECT channel_id from public.user_subscription WHERE subscriber_id = :id",
                params,
                new FollowingIdRowMapper());
        Set<UserEntity> result = new HashSet<>();
        followingIdList.forEach(id -> {
            var followingUser = getById(id);
            if (followingUser.isPresent()) {
                result.add(followingUser.get());
            } else {
                throw new StorageConflictException(Optional.of("Following User #" + id +
                        " for User #" + followerId + " does not exist"));
            }
        });
        return result;
    }

    @Override
    public Set<UserEntity> getFollowerById(UserId followingId) {
        var params = Map.of("id", followingId.toLongValue());
        var followerIdList = jdbcTemplate.query(
                "SELECT subscriber_id from public.user_subscription WHERE channel_id = :id",
                params,
                new FollowerIdRowMapper());
        Set<UserEntity> result = new HashSet<>();
        followerIdList.forEach(id -> {
            var followingUser = getById(id);
            if (followingUser.isPresent()) {
                result.add(followingUser.get());
            } else {
                throw new StorageConflictException(Optional.of("Follower #" + id +
                        " for User #" + followingId + " does not exist"));
            }
        });
        return result;
    }

    @Override
    public void save(UserEntity user) {
        var eventList = user.popEvents();
        if (!eventList.isEmpty()) {
            try {
                if (eventList.stream().anyMatch(UserAddedDomainEvent.class::isInstance)) {
                    this.insert(user);
                } else {
                    this.update(user);
                }
            } catch (DuplicateKeyException ex) {
                throw new StorageConflictException();
            }
        }
        eventPublisher.publish(eventList);

    }

    private void insert(UserEntity user) {
        Map<String, ?> params = Map.of(
                "id", user.id.toLongValue(),
                "login", user.userLogin.toStringValue(),
                "firstName", user.firstName.toStringValue(),
                "patronymicName", user.patronymicName.toStringValue(),
                "lastName", user.lastName.toStringValue(),
                "gender", 1L,
                "isDeleted", user.isDeleted(),
                "version", user.version.toLongValue(),
                "photoId", user.photoId.map(PhotoId::toStringValue).orElse("NULL")
        );
        this.jdbcTemplate.update(
                "INSERT INTO public.users (id,login,first_name,patronymic_name,last_name,deleted,gender_id,version,photo_id)" +
                        "VALUES (:id, :login, :firstName, :patronymicName, :lastName,  :isDeleted, :gender, :version, :photoId)",
                params
        );
    }

    @Override
    public void follow(UserId followingId, UserId currUserId) {
        Map<String, ?> params = Map.of(
                "channel_id", followingId.toLongValue(),
                "subscriber_id", currUserId.toLongValue()
        );
        this.jdbcTemplate.update(
                "INSERT INTO public.user_subscription (channel_id, subscriber_id)" +
                        "VALUES (:channel_id, :subscriber_id)",
                params
        );
    }

    @Override
    public void unfollow(UserId followingId, UserId currUserId) {
        Map<String, ?> params = Map.of(
                "channel_id", followingId.toLongValue(),
                "subscriber_id", currUserId.toLongValue()
        );
        this.jdbcTemplate.update(
                "DELETE FROM public.user_subscription WHERE " +
                        "channel_id = :channel_id AND " +
                        "subscriber_id = :subscriber_id",
                params
        );
    }

    private void update(UserEntity user) {
        Map<String, ?> params = Map.of(
                "id", user.id.toLongValue(),
                "login", user.userLogin.toStringValue(),
                "firstName", user.firstName.toStringValue(),
                "patronymicName", user.patronymicName.toStringValue(),
                "lastName", user.lastName.toStringValue(),
                "gender", 1L,
                "isDeleted", user.isDeleted(),
                "version", user.version.toLongValue(),
                "photoId", user.photoId.map(PhotoId::toStringValue).orElse("NULL")
        );
        int updated = this.jdbcTemplate.update(
                "UPDATE public.users " +
                        "SET id = :id, " +
                        "login = :login, " +
                        "first_name = :firstName, " +
                        "patronymic_name = :patronymicName, " +
                        "last_name = :lastName, " +
                        "deleted = :isDeleted, " +
                        "gender_id = NULL, " +
                        "version = :version, " +
                        "photo_id = :photoId " +
                        "WHERE id = :id",
                params
        );
        if (updated == 0) {
            throw new StorageConflictException(Optional.of("User #" + user.id.toLongValue() + "is outdated"));
        }
    }
}
