package ru.skillbox.microservices.user.persistence.postgres;

import java.util.Optional;

public final class StorageConflictException extends RuntimeException {
    private final String message;

    public StorageConflictException(Optional<String> message) {
        this.message = String.valueOf(message);
    }

    public StorageConflictException() {
        this(Optional.empty());
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
