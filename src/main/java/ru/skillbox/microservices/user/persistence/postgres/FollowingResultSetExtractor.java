package ru.skillbox.microservices.user.persistence.postgres;

import org.springframework.jdbc.core.RowMapper;
import ru.skillbox.microservices.user.domain.UserId;

import java.sql.ResultSet;
import java.sql.SQLException;

class FollowingIdRowMapper implements RowMapper<UserId> {
    @Override
    public UserId mapRow(ResultSet rs, int rowNum) throws SQLException {
        return UserId.fromLong(rs.getLong("channel_id")).get();
    }
}