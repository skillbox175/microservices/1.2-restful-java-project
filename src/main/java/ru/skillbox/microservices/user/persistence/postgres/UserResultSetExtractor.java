package ru.skillbox.microservices.user.persistence.postgres;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import ru.skillbox.microservices.user.common.types.base.Version;
import ru.skillbox.microservices.user.domain.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserResultSetExtractor implements ResultSetExtractor<Optional<UserEntity>> {
    @Override
    public Optional<UserEntity> extractData(ResultSet rs) throws SQLException, DataAccessException {
        if (rs.next()) {
            return Optional.ofNullable(new UserRowMapper().mapRow(rs, 0));
        } else {
            return Optional.empty();
        }
    }
}

class UserRowMapper implements RowMapper<UserEntity> {

    @Override
    public UserEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        var id = rs.getLong("id");
        var login = rs.getString("login");
        var firstName = rs.getString("first_name");
        var patronymicName = rs.getString("patronymic_name");
        var lastName = rs.getString("last_name");
        var isDeleted = rs.getBoolean("deleted");
        var version = rs.getLong("version");
        var photoId = Optional.ofNullable(rs.getString("photo_id"));
        return restoreUser(
                id,
                login,
                firstName,
                patronymicName,
                lastName,
                isDeleted,
                version,
                photoId
        );
    }

    private UserEntity restoreUser(long id,
                                   String login,
                                   String firstName,
                                   String patronymicName,
                                   String lastName,
                                   boolean isDeleted,
                                   long version,
                                   Optional<String> photoId
    ) {
        var userId = UserId
                .fromLong(id)
                .getOrElseThrow(t -> new IllegalStateException("User id " + id + " is incorrect"));
        var userLogin = UserLogin
                .fromString(login)
                .getOrElseThrow(t -> new IllegalStateException("User login " + login + " is incorrect for User #" + id));
        var firstNameValue = UserName.fromString(firstName).getOrElseThrow(t -> new IllegalStateException("User first name " + firstName + " is incorrect for User #" + id));
        var patronymicNameValue = UserName.fromString(patronymicName).getOrElseThrow(t -> new IllegalStateException("User patronymic name " + patronymicName + " is incorrect for User #" + id));
        var lastNameValue = UserName.fromString(lastName).getOrElseThrow(t -> new IllegalStateException("User last name " + lastName + " is incorrect for User #" + id));
        var versionValue = Version.fromLong(version);
        var photoIdValue = photoId.map(it -> PhotoId.fromString(it).getOrNull());
        return UserRestorer.restoreUser(
                userId,
                userLogin,
                firstNameValue,
                patronymicNameValue,
                lastNameValue,
                isDeleted,
                versionValue,
                photoIdValue
        );
    }
}
