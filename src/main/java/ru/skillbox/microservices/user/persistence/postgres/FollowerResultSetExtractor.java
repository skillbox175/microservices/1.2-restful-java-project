package ru.skillbox.microservices.user.persistence.postgres;

import org.springframework.jdbc.core.RowMapper;
import ru.skillbox.microservices.user.domain.UserId;

import java.sql.ResultSet;
import java.sql.SQLException;

class FollowerIdRowMapper implements RowMapper<UserId> {
    @Override
    public UserId mapRow(ResultSet rs, int rowNum) throws SQLException {
        return UserId.fromLong(rs.getLong("subscriber_id")).get();
    }
}