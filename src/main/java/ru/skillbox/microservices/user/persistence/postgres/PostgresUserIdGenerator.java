package ru.skillbox.microservices.user.persistence.postgres;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserIdGenerator;

import javax.sql.DataSource;

public class PostgresUserIdGenerator implements UserIdGenerator {
    private final JdbcTemplate template;

    public PostgresUserIdGenerator(DataSource dataSource) {
        super();
        this.template = new JdbcTemplate(dataSource);
    }


    @Override

    public UserId generate() {
        Long id = template.queryForObject("SELECT nextval('users_id_seq');", Long.class);
        return UserId.fromLong(id).get();
    }
}
