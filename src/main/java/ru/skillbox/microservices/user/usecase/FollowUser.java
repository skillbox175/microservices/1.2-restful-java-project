package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;

public interface FollowUser {
    Either<FollowUserUseCaseError, Void> execute(
            UserId currUserId,
            UserId followingId);

    abstract class FollowUserUseCaseError implements BusinessError {
        private FollowUserUseCaseError() {
        }

        public static FollowUserUseCaseError from(UserEntity.UserEntityError e) {
            if (e instanceof UserEntity.AlreadyFollowingError) return new UserAlreadySubscribed();
            else if (e instanceof UserEntity.NotFoundError ) return new FollowedNotFound();
            else throw new UnknownError();
        }
    }

    final class FollowedNotFound extends FollowUserUseCaseError {
    }

    final class FollowerNotFound extends FollowUserUseCaseError {
    }

    final class UserAlreadySubscribed extends FollowUserUseCaseError {
    }
}