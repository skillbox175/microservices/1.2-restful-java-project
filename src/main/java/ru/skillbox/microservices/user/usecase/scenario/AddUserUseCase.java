package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import ru.skillbox.microservices.user.domain.*;
import ru.skillbox.microservices.user.usecase.AddUser;
import ru.skillbox.microservices.user.usecase.access.UserPersister;

@AllArgsConstructor
public class AddUserUseCase implements AddUser {
    private UserPersister userPersister;
    private UserIdGenerator userIdGenerator;
    private UserAlreadyExists userAlreadyExists;

    @Override
    public Either<AddUserUseCaseError, UserId> execute(
            UserLogin userLogin,
            UserName firstName,
            UserName patronymicName,
            UserName lastName
    ) {
        return UserEntity.addUser(
                        userIdGenerator,
                        userAlreadyExists,
                        userLogin,
                        firstName,
                        patronymicName,
                        lastName
                ).mapLeft(e -> AddUserUseCaseError.from())
                .map(user -> {
                    userPersister.save(user);
                    return user.id;
                });
    }
}

