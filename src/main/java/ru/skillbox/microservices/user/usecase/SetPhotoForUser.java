package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.PhotoId;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

public interface SetPhotoForUser {

    Either<SetPhotoForUserUseCaseError, UserInfo> execute(UserId userId, PhotoId photoId);

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    abstract class SetPhotoForUserUseCaseError implements BusinessError {
        public static SetPhotoForUserUseCaseError from() {
            return new UserNotFound();
        }
    }

    final class UserNotFound extends SetPhotoForUserUseCaseError {
    }
}
