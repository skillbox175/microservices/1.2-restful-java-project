package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

public interface RemovePhotoForUser {

    Either<RemovePhotoForUserUseCaseError, UserInfo> execute(UserId userId);

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    abstract class RemovePhotoForUserUseCaseError implements BusinessError {
        public static RemovePhotoForUserUseCaseError from() {
            return new UserNotFound();
        }
    }

    final class UserNotFound extends RemovePhotoForUserUseCaseError {
    }
}
