package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

public interface UpdateUser {
    Either<UpdateUserUseCaseError, UserInfo> execute(
            UserId userId,
            UserLogin userLogin,
            UserName firstName,
            UserName lastName,
            UserName patronymicName);

    abstract class UpdateUserUseCaseError implements BusinessError {
        private UpdateUserUseCaseError() {
        }

        public static UpdateUserUseCaseError from() {
            return new UpdateUser.UserNotFound();
        }

    }

    final class UserNotFound extends UpdateUserUseCaseError {
    }
}
