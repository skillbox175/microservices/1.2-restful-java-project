package ru.skillbox.microservices.user.usecase.dto;

import ru.skillbox.microservices.user.domain.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public record UserInfo(UserId id, UserLogin login, UserName firstName,
                       UserName patronymicName, UserName lastName, Optional<PhotoId> photoId,
                       Set<UserSimpleInfo> followings, Set<UserSimpleInfo> followers) {

    public static UserInfo from(UserEntity user) {
        return new UserInfo(
                user.id,
                user.userLogin,
                user.firstName,
                user.patronymicName,
                user.lastName,
                user.photoId,
                new HashSet<>(),
                new HashSet<>()
        );
    }

    public static UserInfo from(
            UserEntity user,
            Set<UserEntity> followings,
            Set<UserEntity> followers
    ) {
        Set<UserSimpleInfo> followingsList = new HashSet<>();
        Set<UserSimpleInfo> followersList = new HashSet<>();

        followings.forEach(it -> followingsList.add(UserSimpleInfo.from(it)));
        followers.forEach(it -> followersList.add(UserSimpleInfo.from(it)));

        return new UserInfo(
                user.id,
                user.userLogin,
                user.firstName,
                user.patronymicName,
                user.lastName,
                user.photoId,
                followingsList,
                followersList
        );
    }
}
