package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;

public interface AddUser {
    Either<AddUser.AddUserUseCaseError, UserId> execute(
            UserLogin userLogin,
            UserName firstName,
            UserName lastName,
            UserName patronymicName);

    abstract class AddUserUseCaseError implements BusinessError {
        private AddUserUseCaseError() {
        }

        public static AddUserUseCaseError from() {
            return new AlreadyExists();
        }

    }

    final class AlreadyExists extends AddUserUseCaseError {
    }
}

