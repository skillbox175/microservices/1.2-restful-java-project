package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;

public interface UnfollowUser {
    Either<UnfollowUserUseCaseError, Void> execute(
            UserId currUserId,
            UserId followingId);

    abstract class UnfollowUserUseCaseError implements BusinessError {
        private UnfollowUserUseCaseError() {
        }

        public static UnfollowUserUseCaseError from(UserEntity.UserEntityError e) {
            if (e instanceof UserEntity.NotFollowingError) return new NotFollowing();
            else if (e instanceof UserEntity.NotFoundError) return new UserNotFound();
            else throw new UnknownError();
        }
    }

    final class NotFollowing extends UnfollowUserUseCaseError {
    }

    final class UserNotFound extends UnfollowUserUseCaseError {
    }

    final class FollowerNotFound extends UnfollowUserUseCaseError {
    }
}