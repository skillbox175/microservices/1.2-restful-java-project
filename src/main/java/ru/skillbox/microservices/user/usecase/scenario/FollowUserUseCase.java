package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.domain.UserAlreadyExists;
import ru.skillbox.microservices.user.domain.UserAlreadyFollowing;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.FollowUser;
import ru.skillbox.microservices.user.usecase.access.UserPersister;

@RequiredArgsConstructor
public class FollowUserUseCase implements FollowUser {
    private final UserPersister userPersister;
    private final UserAlreadyFollowing alreadyFollowing;
    private final UserAlreadyExists alreadyExists;

    @Override
    public Either<FollowUserUseCaseError, Void> execute(
            UserId currUserId,
            UserId followingId
    ) {
        if (alreadyFollowing.invoke(followingId, currUserId))
            return Either.left(new UserAlreadySubscribed());

        else if (!alreadyExists.invoke(followingId))
            return Either.left(new FollowedNotFound());

        else {
            userPersister.follow(followingId, currUserId);
            return Either.right(null);
        }
    }
}
