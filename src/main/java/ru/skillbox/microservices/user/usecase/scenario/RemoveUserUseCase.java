package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.RemoveUser;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.access.UserPersister;

import java.util.Optional;

@RequiredArgsConstructor
public class RemoveUserUseCase implements RemoveUser {
    private final UserExtractor userExtractor;
    private final UserPersister userPersister;

    @Override
    public Either<RemoveUserUseCaseError, Void> execute(UserId id) {
        Optional<UserEntity> user = userExtractor.getById(id);
        if (user.isPresent()) {
            user.get().removeUser();
            userPersister.save(user.get());
            return Either.right(null);
        } else {
            return Either.left(new UserNotFound());
        }
    }
}
