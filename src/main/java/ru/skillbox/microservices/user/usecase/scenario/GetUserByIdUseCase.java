package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.GetUserById;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.Optional;

@AllArgsConstructor
public class GetUserByIdUseCase implements GetUserById {
    private UserExtractor userExtractor;

    @Override
    public Either<GetUserByIdUseCaseError, UserInfo> execute(UserId id) {
        Optional<UserEntity> user = userExtractor.getById(id);
        if (user.isPresent() && !user.get().isDeleted()) {
            return Either.right(
                    UserInfo.from(
                            user.get(),
                            userExtractor.getFollowingById(id),
                            userExtractor.getFollowerById(id)
                    ));
        } else {
            return Either.left(new UserNotFound());
        }
    }
}
