package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.domain.UserAlreadyExists;
import ru.skillbox.microservices.user.domain.UserAlreadyFollowing;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.UnfollowUser;
import ru.skillbox.microservices.user.usecase.access.UserPersister;

@RequiredArgsConstructor
public class UnfollowUserUseCase implements UnfollowUser {
    private final UserPersister userPersister;
    private final UserAlreadyFollowing alreadyFollowing;
    private final UserAlreadyExists alreadyExists;

    @Override
    public Either<UnfollowUserUseCaseError, Void> execute(
            UserId currUserId,
            UserId followingId
    ) {
        if (!alreadyExists.invoke(followingId))
            return Either.left(new UserNotFound());

        else if (!alreadyFollowing.invoke(followingId, currUserId))
            return Either.left(new NotFollowing());

        else {
            userPersister.unfollow(followingId, currUserId);
            return Either.right(null);
        }
    }
}
