package ru.skillbox.microservices.user.usecase.invariant;

import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.domain.UserAlreadyFollowing;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class UserAlreadyFollowingUsingUserExtractor implements UserAlreadyFollowing {
    private final UserExtractor extractor;

    @Override
    public boolean invoke(UserId followingId, UserId currUserId) {
        var followingUserList = extractor.getFollowingById(currUserId);
        var result =
                followingUserList.stream().map(it -> it.id).collect(Collectors.toSet());
        return result.contains(followingId);
    }
}
