package ru.skillbox.microservices.user.usecase.access;

import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;

public interface UserPersister {
    void save(UserEntity user);

    void follow(UserId followingId, UserId currUserId);

    void unfollow(UserId followingId, UserId currUserId);
}
