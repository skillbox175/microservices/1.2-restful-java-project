package ru.skillbox.microservices.user.usecase.access;

import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserExtractor {
    Optional<UserEntity> getById(UserId id);

    Optional<UserEntity> getByLogin(UserLogin login);

    List<UserEntity> getAll();

    Set<UserEntity> getFollowingById(UserId followerId);

    Set<UserEntity> getFollowerById(UserId followerId);
}
