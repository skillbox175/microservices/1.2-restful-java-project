package ru.skillbox.microservices.user.usecase.dto;

import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;

public record UserSimpleInfo(UserId id, UserLogin login, UserName firstName,
                             UserName patronymicName, UserName lastName) {

    public static UserSimpleInfo from(UserEntity user) {
        return new UserSimpleInfo(
                user.id,
                user.userLogin,
                user.firstName,
                user.patronymicName,
                user.lastName
        );
    }

    public static UserSimpleInfo from(UserInfo user) {
        return new UserSimpleInfo(
                user.id(),
                user.login(),
                user.firstName(),
                user.patronymicName(),
                user.lastName()
        );
    }

}
