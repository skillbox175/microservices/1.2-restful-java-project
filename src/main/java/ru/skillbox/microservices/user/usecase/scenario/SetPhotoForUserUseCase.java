package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import ru.skillbox.microservices.user.domain.PhotoId;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.SetPhotoForUser;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.access.UserPersister;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.Optional;

@AllArgsConstructor
public class SetPhotoForUserUseCase implements SetPhotoForUser {
    private UserExtractor userExtractor;
    private UserPersister userPersister;

    @Override
    public Either<SetPhotoForUserUseCaseError, UserInfo> execute(UserId id, PhotoId photoId) {
        Optional<UserEntity> user = userExtractor.getById(id);
        if (user.isEmpty() || user.get().isDeleted()) {
            return Either.left(new SetPhotoForUser.UserNotFound());
        }

        return user.get().setPhoto(photoId)
                .mapLeft(e -> SetPhotoForUserUseCaseError.from())
                .map(u -> {
                    userPersister.save(u);
                    return UserInfo.from(u);
                });
    }
}
