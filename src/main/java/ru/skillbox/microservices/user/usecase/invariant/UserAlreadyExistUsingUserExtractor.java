package ru.skillbox.microservices.user.usecase.invariant;

import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.domain.UserAlreadyExists;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;

import java.util.Optional;

@RequiredArgsConstructor
public class UserAlreadyExistUsingUserExtractor implements UserAlreadyExists {
    private final UserExtractor extractor;

    @Override
    public boolean invoke(UserLogin userLogin) {
        Optional<UserEntity> user = extractor.getByLogin(userLogin);
        return user.isPresent() && !user.get().isDeleted();
    }

    @Override
    public boolean invoke(UserId userId) {
        Optional<UserEntity> user = extractor.getById(userId);
        return user.isPresent() && !user.get().isDeleted();
    }
}
