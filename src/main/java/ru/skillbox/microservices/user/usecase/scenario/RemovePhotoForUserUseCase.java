package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.RemovePhotoForUser;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.access.UserPersister;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.Optional;

@AllArgsConstructor
public class RemovePhotoForUserUseCase implements RemovePhotoForUser {
    private UserExtractor userExtractor;
    private UserPersister userPersister;

    @Override
    public Either<RemovePhotoForUserUseCaseError, UserInfo> execute(UserId id) {
        Optional<UserEntity> user = userExtractor.getById(id);
        if (user.isEmpty() || user.get().isDeleted()) {
            return Either.left(new RemovePhotoForUserUseCase.UserNotFound());
        }

        return user.get().removePhoto()
                .mapLeft(e -> RemovePhotoForUserUseCaseError.from())
                .map(u -> {
                    userPersister.save(u);
                    return UserInfo.from(u);
                });
    }
}
