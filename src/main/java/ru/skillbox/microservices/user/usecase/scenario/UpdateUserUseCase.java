package ru.skillbox.microservices.user.usecase.scenario;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;
import ru.skillbox.microservices.user.usecase.UpdateUser;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.access.UserPersister;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.Optional;

@AllArgsConstructor
public class UpdateUserUseCase implements UpdateUser {

    private final UserPersister userPersister;
    private final UserExtractor userExtractor;

    @Override
    public Either<UpdateUserUseCaseError, UserInfo> execute(
            UserId id,
            UserLogin userLogin,
            UserName firstName,
            UserName patronymicName,
            UserName lastName
    ) {
        Optional<UserEntity> user = userExtractor.getById(id);
        if (user.isEmpty() || user.get().isDeleted()) {
            return Either.left(new UpdateUserUseCase.UserNotFound());
        }

        return user.get().updateUser(
                        userLogin,
                        firstName,
                        patronymicName,
                        lastName
                ).mapLeft(e -> UpdateUserUseCaseError.from())
                .map(u -> {
                    userPersister.save(u);
                    return UserInfo.from(u);
                });
    }
}