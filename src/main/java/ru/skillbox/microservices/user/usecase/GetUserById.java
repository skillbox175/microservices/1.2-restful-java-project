package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

public interface GetUserById {
    Either<GetUserByIdUseCaseError, UserInfo> execute(UserId id);

    abstract class GetUserByIdUseCaseError implements BusinessError {
        private GetUserByIdUseCaseError() {}

    }

    final class UserNotFound extends GetUserByIdUseCaseError {
    }
}
