package ru.skillbox.microservices.user.usecase.scenario;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.usecase.GetAllUsers;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class GetAllUsersUseCase implements GetAllUsers {
    private UserExtractor userExtractor;

    @Override
    public List<UserInfo> execute() {
        var userList = userExtractor.getAll()
                .stream().map(UserInfo::from).toList();
        var followingList =
                userList.stream().map(it -> userExtractor.getFollowingById(it.id())).toList();
        var followerList =
                userList.stream().map(it -> userExtractor.getFollowerById(it.id())).toList();
        int i = 0;
        List<UserInfo> result = new ArrayList<>();
        for (UserInfo user : userList) {
            result.add(UserInfo.from(
                    UserEntity.builder()
                            .id(user.id())
                            .userLogin(user.login())
                            .firstName(user.firstName())
                            .patronymicName(user.patronymicName())
                            .lastName(user.lastName())
                            .build(),
                    followingList.get(i),
                    followerList.get(i))
            );
            i++;
        }
        return result;
    }
}
