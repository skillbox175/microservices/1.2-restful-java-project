package ru.skillbox.microservices.user.usecase;

import io.vavr.control.Either;
import ru.skillbox.microservices.user.common.types.error.BusinessError;
import ru.skillbox.microservices.user.domain.UserId;

public interface RemoveUser {
    Either<RemoveUserUseCaseError, Void> execute(UserId id);

    abstract class RemoveUserUseCaseError implements BusinessError {
        private RemoveUserUseCaseError() {

        }

    }

    final class UserNotFound extends RemoveUserUseCaseError {

    }
}
