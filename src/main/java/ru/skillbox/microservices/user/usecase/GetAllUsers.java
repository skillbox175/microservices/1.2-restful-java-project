package ru.skillbox.microservices.user.usecase;

import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.List;

public interface GetAllUsers {
    List<UserInfo> execute();
}
