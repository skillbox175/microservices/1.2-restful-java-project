package ru.skillbox.microservices.user.application.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.skillbox.microservices.user.common.events.DomainEventListener;
import ru.skillbox.microservices.user.common.events.DomainEventPublisher;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;

import java.util.*;

public class EventPublisherImpl implements DomainEventPublisher {

    private final Logger logger = LoggerFactory.getLogger(EventPublisherImpl.class);
    private final Map<Class<?>, List<DomainEventListener<? extends DomainEvent>>> listenerMap = new HashMap<>();

    /**
     * Register a listener for a specific domain event type.
     *
     * @param  listener   the listener to be registered
     */
    public void registerListener(DomainEventListener<? extends DomainEvent> listener) {
        // Compute the event type of the listener and retrieve the corresponding list of listeners
        listenerMap.compute(listener.eventType(), (key, value) -> {
            // If the list of listeners is null, create a new one
            List<DomainEventListener<? extends DomainEvent>> list = value != null ? value : new ArrayList<>();

            list.add(listener);
            return list;
        });
    }

    /**
     * A method to publish a collection of domain events.
     *
     * @param  events  the collection of domain events to publish
     */
    public void publish(Collection<? extends DomainEvent> events) {
        events.forEach(e -> {
            logger.info("Processing event: {}", e);

            // Retrieve the list of listeners for the event's class
            List<DomainEventListener<? extends DomainEvent>> listeners = listenerMap.get(e.getClass());

            // If there are listeners for the event, send the event to them
            if (listeners != null) {
                sendEvents(listeners, e);
            }
        });
    }

    /**
     * A method to send events to a list of event listeners.
     *
     * @param  listeners  the list of event listeners
     * @param  event      the event to be sent
     */
    private void sendEvents(List<DomainEventListener<? extends DomainEvent>> listeners, DomainEvent event) {
        listeners.forEach(l -> l.handle(event));
    }
}
