package ru.skillbox.microservices.user.application.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.skillbox.microservices.user.application.configuration.legacy.LegacyConfiguration;
import ru.skillbox.microservices.user.application.configuration.new_gen.ApplicationNewGenConfiguration;

@Configuration
@Import(
        {
                ApplicationNewGenConfiguration.class,
                LegacyConfiguration.class,
                SwaggerConfiguration.class,
                KafkaConfiguration.class
        }
)
@EnableAutoConfiguration
public class ApplicationConfiguration {

}
