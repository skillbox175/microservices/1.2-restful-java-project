package ru.skillbox.microservices.user.application.event;

import ru.skillbox.microservices.user.common.types.base.DomainEvent;

public interface EventPublisher {
    void publish(DomainEvent event);
}
