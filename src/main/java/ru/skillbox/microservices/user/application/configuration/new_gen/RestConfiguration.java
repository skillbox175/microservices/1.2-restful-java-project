package ru.skillbox.microservices.user.application.configuration.new_gen;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.skillbox.microservices.user.rest.v1.*;
import ru.skillbox.microservices.user.usecase.*;

@Configuration
public class RestConfiguration {
    @Bean
    public AddUserEndpoint addUserEndpoint(AddUser addUser) {
        return new AddUserEndpoint(addUser);
    }

    @Bean
    public UpdateUserEndpoint updateUserEndpoint(UpdateUser updateUser) {
        return new UpdateUserEndpoint(updateUser);
    }

    @Bean
    public GetUserByIdEndpoint getUserByIdEndpoint(GetUserById getUserById) {
        return new GetUserByIdEndpoint(getUserById);
    }

    @Bean
    public RemoveUserEndpoint removeUserEndpoint(RemoveUser removeUser) {
        return new RemoveUserEndpoint(removeUser);
    }

    @Bean
    public GetAllUsersEndpoint getAllUsersEndpoint(GetAllUsers getAllUsers) {
        return new GetAllUsersEndpoint(getAllUsers);
    }

    @Bean
    public FollowUserEndpoint followUserEndpoint(FollowUser followUser) {
        return new FollowUserEndpoint(followUser);
    }

    @Bean
    public UnfollowUserEndpoint unfollowUserEndpoint(UnfollowUser unfollowUser) {
        return new UnfollowUserEndpoint(unfollowUser);
    }
}
