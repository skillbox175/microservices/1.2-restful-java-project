package ru.skillbox.microservices.user.application.configuration.new_gen;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.skillbox.microservices.user.domain.UserAlreadyExists;
import ru.skillbox.microservices.user.domain.UserAlreadyFollowing;
import ru.skillbox.microservices.user.domain.UserIdGenerator;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.access.UserPersister;
import ru.skillbox.microservices.user.usecase.invariant.UserAlreadyExistUsingUserExtractor;
import ru.skillbox.microservices.user.usecase.invariant.UserAlreadyFollowingUsingUserExtractor;
import ru.skillbox.microservices.user.usecase.scenario.*;

@Configuration
public class UseCaseConfiguration {
    @Bean
    public AddUserUseCase addUserUseCase(
            UserPersister userPersister,
            UserIdGenerator idGenerator,
            UserAlreadyExists alreadyExist) {
        return new AddUserUseCase(
                userPersister,
                idGenerator,
                alreadyExist
        );
    }

    @Bean
    public UpdateUserUseCase updateUserUseCase(
            UserPersister userPersister,
            UserExtractor userExtractor
    ) {
        return new UpdateUserUseCase(
                userPersister,
                userExtractor
        );
    }

    @Bean
    public GetUserByIdUseCase getUserByIdUseCase(UserExtractor userExtractor) {
        return new GetUserByIdUseCase(userExtractor);
    }

    @Bean
    public UserAlreadyExists userAlreadyExists(UserExtractor userExtractor) {
        return new UserAlreadyExistUsingUserExtractor(userExtractor);
    }

    @Bean
    public RemoveUserUseCase removeUserUseCase(
            UserExtractor userExtractor,
            UserPersister userPersister
    ) {
        return new RemoveUserUseCase(userExtractor, userPersister);
    }

    @Bean
    public GetAllUsersUseCase getAllUsersUseCase(UserExtractor userExtractor) {
        return new GetAllUsersUseCase(userExtractor);
    }

    @Bean
    public FollowUserUseCase followUserUseCase(
            UserPersister userPersister,
            UserAlreadyFollowing alreadyFollowing,
            UserAlreadyExists alreadyExists
    ) {
        return new FollowUserUseCase(
                userPersister, alreadyFollowing, alreadyExists
        );
    }

    @Bean
    public UnfollowUserUseCase unfollowUserUseCase(
            UserPersister userPersister,
            UserAlreadyFollowing userAlreadyFollowing,
            UserAlreadyExists userAlreadyExists
    ) {
        return new UnfollowUserUseCase(
                userPersister, userAlreadyFollowing, userAlreadyExists
        );
    }

    @Bean
    public UserAlreadyFollowing alreadyFollowing(UserExtractor extractor) {
        return new UserAlreadyFollowingUsingUserExtractor(extractor);
    }

    @Bean
    public SetPhotoForUserUseCase setPhotoForUserUseCase(UserExtractor userExtractor, UserPersister userPersister) {
        return new SetPhotoForUserUseCase(userExtractor, userPersister);
    }

    @Bean
    public RemovePhotoForUserUseCase removePhotoForUserUseCase(UserExtractor userExtractor, UserPersister userPersister) {
        return new RemovePhotoForUserUseCase(userExtractor, userPersister);
    }
}
