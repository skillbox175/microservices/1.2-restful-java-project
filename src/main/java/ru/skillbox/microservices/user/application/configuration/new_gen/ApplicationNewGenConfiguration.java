package ru.skillbox.microservices.user.application.configuration.new_gen;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(
        {
                PersistenceConfiguration.class,
                RestConfiguration.class,
                UseCaseConfiguration.class
        }
)
@EnableAutoConfiguration
public class ApplicationNewGenConfiguration {
}

