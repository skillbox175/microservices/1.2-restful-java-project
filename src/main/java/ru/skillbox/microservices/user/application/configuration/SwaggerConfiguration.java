package ru.skillbox.microservices.user.application.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public GroupedOpenApi apiV1() {
        return GroupedOpenApi.builder()
                .group("api v1")
                .packagesToScan("ru.skillbox.microservices.user.rest.v1")
                .pathsToMatch("/**")
                .build();
    }
    @Bean
    public GroupedOpenApi apiLegacy() {
        return GroupedOpenApi.builder()
                .group("legacy")
                .packagesToScan("ru.skillbox.microservices.user.legacy.controller")
                .pathsToMatch("/**")
                .build();
    }


    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("User Service REST API").version("v1.0.0"));
    }

}