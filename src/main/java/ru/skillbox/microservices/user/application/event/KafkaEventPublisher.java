package ru.skillbox.microservices.user.application.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import ru.skillbox.microservices.user.common.events.DomainEventPublisher;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;

import java.util.Collection;


@Component
@RequiredArgsConstructor
public class KafkaEventPublisher implements DomainEventPublisher {
    private Logger logger = LoggerFactory.getLogger(KafkaEventPublisher.class);

    static final String TOPIC_NAME = "userEvents";

    private final KafkaTemplate<String, String> userEventsKafkaTemplate;

    @Override
    @SneakyThrows
    public void publish(Collection<? extends DomainEvent> events) {
        events.forEach(e -> {
            logger.info("Processing event: {}", e);
            sendEvent(e);
        });
    }

    @SneakyThrows
    private void sendEvent(DomainEvent event) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.registerModule(new JavaTimeModule());
        String payload = mapper.writeValueAsString(event);
        String eventName = event.getClass().getSimpleName();
        logger.info("Sending event '{}' with payload='{}' to topic='{}'", eventName, payload, TOPIC_NAME);
        userEventsKafkaTemplate.send(TOPIC_NAME, eventName, payload);
    }
}
