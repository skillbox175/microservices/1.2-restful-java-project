package ru.skillbox.microservices.user.application.configuration.legacy;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "ru.skillbox.microservices.user.legacy.repository")
@ComponentScan(basePackages = {
        "ru.skillbox.microservices.user.legacy.repository",
        "ru.skillbox.microservices.user.legacy.service",
        "ru.skillbox.microservices.user.legacy.controller"
})
@EntityScan(basePackages = "ru.skillbox.microservices.user.legacy.entity")
public class LegacyConfiguration {

}
