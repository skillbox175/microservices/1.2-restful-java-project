package ru.skillbox.microservices.user.application.configuration.new_gen;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.skillbox.microservices.user.common.events.DomainEventPublisher;
import ru.skillbox.microservices.user.domain.UserIdGenerator;
import ru.skillbox.microservices.user.persistence.postgres.PostgresUserIdGenerator;
import ru.skillbox.microservices.user.persistence.postgres.PostgresUserRepository;

import javax.sql.DataSource;

@Configuration
public class PersistenceConfiguration {
    @Bean
    public PostgresUserRepository userRepository(DataSource dataSource, DomainEventPublisher eventPublisher) {
        return new PostgresUserRepository(dataSource, eventPublisher);
    }

    @Bean
    public UserIdGenerator userIdGenerator(DataSource dataSource) {
        return new PostgresUserIdGenerator(dataSource);
    }

}
