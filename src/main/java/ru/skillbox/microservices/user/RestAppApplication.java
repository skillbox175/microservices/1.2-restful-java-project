package ru.skillbox.microservices.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ru.skillbox.microservices.user.application.configuration.ApplicationConfiguration;

@SpringBootApplication
@EnableConfigurationProperties
public class RestAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationConfiguration.class, args);
    }
}
//public class RestAppApplication implements CommandLineRunner{

//    public static void main(String[] args) {
//        SpringApplication.run(RestAppApplication.class, args);
//    }

//    @Autowired
//    UserRepository userRepository;
//
//    @Override
//    public void run(String... args) throws Exception {
//        User petya = new User("Petya", "Ivanov");
//        User vanya = new User("Vanya", "Sidorov");
//
//        List<User> people = Arrays.asList(petya, vanya);
//
//
//        userRepository.saveAll(people);
//        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<"+userRepository.findAll());
//    }

//}
