package ru.skillbox.microservices.user.legacy.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.skillbox.microservices.user.legacy.entity.User;
import ru.skillbox.microservices.user.legacy.service.UserService;

@Slf4j
@RestController
@RequestMapping(value = "/users")
@RequiredArgsConstructor
@Controller
public class UserController {

    @Autowired
    private final UserService userService;

    @PostMapping
    @Operation(summary = "Добавление пользователя", tags = "users")
    @ApiResponse(responseCode = "200", description = "Пользователь {lastName} добавлен с id = {id}")
    String createUser(@RequestBody User user) {
        log.info("Запрос на создание пользователя {}", user.getLogin());
        return userService.createUser(user);
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Получение пользователя", tags = "users")
    User getUser(@PathVariable long id) {
        return userService.getUser(id);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Обновление пользователя", tags = "users")
    @ApiResponse(responseCode = "200", description = "Пользователь {lastName} успешно сохранен")
    String updateUser(@RequestBody User user, @PathVariable long id) {
        return userService.updateUser(user, id);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление пользователя", tags = "users")
    @ApiResponse(responseCode = "200", description = "Пользователь с id = {id} успешно удален")
    String deleteUser(@PathVariable long id) {
        return userService.deleteUser(id);
    }

    @GetMapping
    @Parameter(in = ParameterIn.QUERY, name = "isDeleted", hidden = true)
    @Operation(summary = "Получение списка пользователей", tags = "users")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = User.class)))
    public Iterable<User> findAll() {
        return userService.findAll();
    }

    @PostMapping(value = "/{id}/subscribe")
    @Operation(summary = "Добавление подписчика", tags = "users")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = User.class)))
    String subscribeToUser(@RequestBody User subscriber, @PathVariable long id) {
        return userService.subscribeToUser(subscriber, id);
    }

    @PostMapping(value = "/{id}/unsubscribe")
    @Operation(summary = "Удаление подписчика", tags = "users")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = User.class)))
    String unsubscribeFromUser(@RequestBody User subscriber, @PathVariable long id) {
        return userService.unsubscribeFromUser(subscriber, id);
    }


}
