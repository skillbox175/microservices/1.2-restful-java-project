package ru.skillbox.microservices.user.legacy.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "users")
@SQLDelete(sql = "UPDATE users SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Уникальный идентификатор пользователя", example = "1")
    private Long id;

    @EqualsAndHashCode.Include
    @Schema(description = "User's unique login", example = "vanya.ivanov")
    private String login;

    @EqualsAndHashCode.Include
    @Schema(description = "Имя пользователя", example = "Vanya")
    private String firstName;

    @EqualsAndHashCode.Include
    @Schema(description = "Фамилия пользователя", example = "Ivanov")
    private String lastName;

    @EqualsAndHashCode.Include
    @Schema(description = "Отчество пользователя", example = "Petrovich")
    private String patronymicName;

    @ManyToMany
    @JoinTable(
            name = "user_subscription",
            joinColumns = {@JoinColumn(name = "channel_id")},
            inverseJoinColumns = {@JoinColumn(name = "subscriber_id")})
    @JsonIgnoreProperties({"subscribers", "subscriptions"})
    @ToString.Exclude
    private Set<User> subscribers = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "user_subscription",
            joinColumns = {@JoinColumn(name = "subscriber_id")},
            inverseJoinColumns = {@JoinColumn(name = "channel_id")})
    @JsonIgnoreProperties({"subscribers", "subscriptions"})
    @ToString.Exclude
    private Set<User> subscriptions = new HashSet<>();

    @Schema(hidden = true)
    @ToString.Exclude
    private Boolean deleted = Boolean.FALSE;

    public User(String login, String firstName, String lastName, String patronymicName) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymicName = patronymicName;
    }

    public User(Long id, String login, String firstName, String lastName, String patronymicName) {
        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymicName = patronymicName;
    }

    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymicName='" + patronymicName + '\'' +
                '}';
    }
}
