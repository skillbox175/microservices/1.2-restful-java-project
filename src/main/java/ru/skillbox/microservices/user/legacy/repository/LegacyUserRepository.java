package ru.skillbox.microservices.user.legacy.repository;

import org.springframework.data.repository.CrudRepository;
import ru.skillbox.microservices.user.legacy.entity.User;

import java.util.List;

public interface LegacyUserRepository extends CrudRepository<User, Long> {
    List<User> findByLastName(String lastName);

    List<User> findAll();
}
