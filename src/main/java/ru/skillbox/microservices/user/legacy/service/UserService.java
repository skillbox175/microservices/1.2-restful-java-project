package ru.skillbox.microservices.user.legacy.service;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.microservices.user.legacy.entity.User;
import ru.skillbox.microservices.user.legacy.repository.LegacyUserRepository;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public class UserService {

    private LegacyUserRepository userRepository;

    private EntityManager entityManager;

    @Autowired
    public UserService(LegacyUserRepository legacyUserRepository) {
        this.userRepository = legacyUserRepository;
    }

    public String createUser(User user) {
        User savedUser = userRepository.save(user);
        return String.format("Пользователь %s добавлен в базу с id = %s",
                savedUser.getLogin(),
                savedUser.getId());
    }

    public User getUser(long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public String updateUser(User user, long id) {
        if (!userRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        User savedUser = userRepository.save(user);
        return String.format("Пользователь %s успешно сохранен", savedUser.getLogin());
    }

    public String deleteUser(long id) {
        if (!userRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        userRepository.deleteById(id);
        return String.format("Пользователь %s успешно удален", user.getLogin());
    }

    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    public String subscribeToUser(User subscriber, long userId) {
        Long subscriberId = subscriber.getId();
        User userBeingSubscribedTo = userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        subscriber = userRepository.findById(subscriberId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        userBeingSubscribedTo.getSubscribers().add(subscriber);
        userRepository.save(userBeingSubscribedTo);

        return String.format("Подписчик %s успешно добавлен для пользователя %s", subscriber.getLogin(),
                userBeingSubscribedTo.getLogin());
    }

    public String unsubscribeFromUser(User subscriber, long userId) {
        Long subscriberId = subscriber.getId();
        subscriber = userRepository.findById(subscriberId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        User userBeingUnsubscribedFrom = userRepository.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        userBeingUnsubscribedFrom.getSubscribers().remove(subscriber);

        userRepository.save(userBeingUnsubscribedFrom);

        return String.format("Подписчик %s успешно удален для пользователя %s", subscriber.getLogin(),
                userBeingUnsubscribedFrom.getLogin());
    }
}
