package ru.skillbox.microservices.user.rest.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.dto.UserModel;
import ru.skillbox.microservices.user.rest.v1.validation.UserIdValidator;
import ru.skillbox.microservices.user.usecase.GetUserById;

import java.util.ArrayList;

import static org.springframework.http.ResponseEntity.ok;
import static ru.skillbox.microservices.user.common.rest.RestResponses.resourceNotFound;
import static ru.skillbox.microservices.user.common.rest.RestResponses.toInvalidParamsBadRequest;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_GET_BY_ID;

@RestController
@AllArgsConstructor
@Getter
public class GetUserByIdEndpoint {

    private GetUserById getUserById;

    static ResponseEntity<Problem> toRestError(GetUserById.GetUserByIdUseCaseError error) {
        return resourceNotFound();
    }

    @GetMapping(path = {API_V1_USERS_GET_BY_ID})
    @Operation(summary = "Get user by id", tags = "Users")
    @ApiResponse(
            responseCode = "200",
            content = @Content(schema = @Schema(implementation = UserModel.class))
    )
    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(mediaType = "error", schema = @Schema(implementation = ResponseEntity.class)))
    public ResponseEntity<?> execute(
            @PathVariable
            @Parameter(name = "id", description = "User's unique id", example = "8")
            Long id) {
        var errorList = new ArrayList<ValidationError>();

        var userId = UserIdValidator.validated(id, errorList);

        if (errorList.isEmpty()) {
            return getUserById.execute(userId.get())
                    .fold(
                            (GetUserByIdEndpoint::toRestError),
                            (userInfo -> ok(UserModel.from(userInfo)))
                    );
        } else {
            return toInvalidParamsBadRequest(errorList);
        }
    }
}
