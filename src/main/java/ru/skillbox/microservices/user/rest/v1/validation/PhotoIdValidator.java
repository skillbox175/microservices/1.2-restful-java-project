package ru.skillbox.microservices.user.rest.v1.validation;

import io.vavr.control.Either;
import lombok.experimental.UtilityClass;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.domain.PhotoId;

import java.util.List;

@UtilityClass
public class PhotoIdValidator {
    public static Either<Boolean, PhotoId> validated(String value, List<ValidationError> errorList) {
        Either<PhotoId.CreatePhotoIdError, PhotoId> photoId = PhotoId.fromString(value);
        var error = photoId.mapLeft(it -> {
            if (it instanceof PhotoId.WrongStringValue) {
                return new ValidationError("Photo Id has wrong string value");
            } else if (it instanceof PhotoId.EmptyStringValue) {
                return new ValidationError("Photo Id has an empty string value");
            }
            return null;
        });
        return error.mapLeft(errorList::add);
    }
}
