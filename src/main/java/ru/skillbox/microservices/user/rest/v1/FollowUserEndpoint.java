package ru.skillbox.microservices.user.rest.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.validation.UserIdValidator;
import ru.skillbox.microservices.user.usecase.FollowUser;

import java.util.ArrayList;

import static ru.skillbox.microservices.user.common.rest.RestResponses.*;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_FOLLOW_BY_ID;

@RestController
@AllArgsConstructor
public class FollowUserEndpoint {

    private FollowUser followUser;

    static ResponseEntity<Problem> toRestError(FollowUser.FollowUserUseCaseError error) {
        if (error instanceof FollowUser.FollowedNotFound) return resourceNotFound();
        else if (error instanceof FollowUser.UserAlreadySubscribed)
            return restBusinessError("User already subscribed", "already_subscribed");
        return restBusinessError("Unknown error", "unknown_error");
    }

    @PostMapping(path = {API_V1_USERS_FOLLOW_BY_ID})
    @Operation(summary = "Follow a user", tags = "Users")
    public ResponseEntity<?> execute(
            @PathVariable
            @Parameter(name = "followingId", description = "User's unique id", example = "8")
            Long followingId,
            @PathVariable
            @Parameter(name = "followerId", description = "User's unique id", example = "2")
            Long followerId
    ) {
        var errorList = new ArrayList<ValidationError>();
        var followingIdValidated = UserIdValidator.validated(followingId, errorList);
        var followerIdValidated = UserIdValidator.validated(followerId, errorList);


        if (errorList.isEmpty()) {
            return followUser.execute(followerIdValidated.get(), followingIdValidated.get())
                    .fold(
                            (FollowUserEndpoint::toRestError),
                            (ResponseEntity::ok)
                    );
        } else return toInvalidParamsBadRequest(errorList);


    }
}
