package ru.skillbox.microservices.user.rest.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.validation.UserIdValidator;
import ru.skillbox.microservices.user.usecase.UnfollowUser;

import java.util.ArrayList;

import static ru.skillbox.microservices.user.common.rest.RestResponses.*;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_UNFOLLOW_BY_ID;

@RestController
@AllArgsConstructor
public class UnfollowUserEndpoint {

    private UnfollowUser unfollowUser;

    static ResponseEntity<Problem> toRestError(UnfollowUser.UnfollowUserUseCaseError error) {
        if (error instanceof UnfollowUser.NotFollowing)
            return restBusinessError("User is not following", "not_following");
        else if (error instanceof UnfollowUser.UserNotFound)
            return resourceNotFound();
        return restBusinessError("Unknown error", "unknown_error");
    }

    @PostMapping(path = {API_V1_USERS_UNFOLLOW_BY_ID})
    @Operation(summary = "Unfollow a user", tags = "Users")
    public ResponseEntity<?> execute(
            @PathVariable
            @Parameter(name = "followingId", description = "User's unique id", example = "8")
            Long followingId,
            @PathVariable
            @Parameter(name = "followerId", description = "User's unique id", example = "2")
            Long followerId
    ) {
        var errorList = new ArrayList<ValidationError>();
        var followingIdValidated = UserIdValidator.validated(followingId, errorList);
        var followerIdValidated = UserIdValidator.validated(followerId, errorList);


        if (errorList.isEmpty()) {
            return unfollowUser.execute(followerIdValidated.get(),
                            followingIdValidated.get())
                    .fold(
                            (UnfollowUserEndpoint::toRestError),
                            (ResponseEntity::ok)
                    );
        } else return toInvalidParamsBadRequest(errorList);

    }
}
