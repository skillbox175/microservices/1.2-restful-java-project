package ru.skillbox.microservices.user.rest.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.validation.UserIdValidator;
import ru.skillbox.microservices.user.usecase.RemoveUser;

import java.util.ArrayList;

import static ru.skillbox.microservices.user.common.rest.RestResponses.*;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_DELETE_BY_ID;

@RestController
@AllArgsConstructor
@Getter
public class RemoveUserEndpoint {
    private RemoveUser removeUser;

    static ResponseEntity<Problem> toRestError(RemoveUser.RemoveUserUseCaseError error) {
        return resourceNotFound();
    }

    @DeleteMapping(path = {API_V1_USERS_DELETE_BY_ID})
    @Operation(summary = "Remove a user", tags = "Users")
    @ApiResponse(responseCode = "204")
    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(mediaType = "error", schema = @Schema(implementation = ResponseEntity.class)))
    public ResponseEntity<?> execute(
            @PathVariable
            @Parameter(name = "id", description = "User's unique id", example = "8")
            Long id) {
        var errorList = new ArrayList<ValidationError>();

        var userId = UserIdValidator.validated(id, errorList);

        if (errorList.isEmpty()) {
            return removeUser.execute(userId.get())
                    .fold(
                            (RemoveUserEndpoint::toRestError),
                            (result -> noContent())
                    );
        } else {
            return toInvalidParamsBadRequest(errorList);
        }
    }

}
