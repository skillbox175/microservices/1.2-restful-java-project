package ru.skillbox.microservices.user.rest.v1;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EndpointUrl {
    public static final String API_V1 = "/rest/users/v1";
    public static final String API_V1_USERS = API_V1 + "/users";
    public static final String API_V1_USERS_ADD = API_V1_USERS;
    public static final String API_V1_USERS_UPDATE = API_V1_USERS;
    public static final String API_V1_USERS_GET_BY_ID = API_V1_USERS + "/{id}";
    public static final String API_V1_USERS_GET_ALL = API_V1_USERS;
    public static final String API_V1_USERS_DELETE_BY_ID = API_V1_USERS + "/{id}";
    public static final String API_V1_USERS_FOLLOW_BY_ID = API_V1_USERS +
            "/{followingId}/follow/{followerId}";
    public static final String API_V1_USERS_UNFOLLOW_BY_ID = API_V1_USERS +
            "/{followingId}/unfollow/{followerId}";
    public static final String API_V1_USERS_SET_PHOTO = API_V1_USERS + "/{userId}/photo/{photoId}";
    public static final String API_V1_USERS_REMOVE_PHOTO = API_V1_USERS + "/{userId}/photo";
}
