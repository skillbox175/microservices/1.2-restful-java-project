package ru.skillbox.microservices.user.rest.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.validation.UserLoginValidator;
import ru.skillbox.microservices.user.rest.v1.validation.UserNameValidator;
import ru.skillbox.microservices.user.usecase.AddUser;

import java.util.ArrayList;

import static ru.skillbox.microservices.user.common.rest.RestResponses.*;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_ADD;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_GET_BY_ID;

@RestController
@AllArgsConstructor
@Getter
public class AddUserEndpoint {

    private AddUser addUser;

    static @NonNull ResponseEntity<Problem> toRestError(AddUser.AddUserUseCaseError error) {
        return restBusinessError("User already exist", "already_exists");
    }

    @PostMapping(path = {API_V1_USERS_ADD})
    @Operation(summary = "Create a user", tags = "Users")
    @ApiResponse(responseCode = "200", headers = @Header(name = "Location"))
    public ResponseEntity<?> execute(@RequestBody AddUserRestRequest request) {
        var errorList = new ArrayList<ValidationError>();

        var login = UserLoginValidator.validated(request.login, errorList);
        var firstName = UserNameValidator.validated(request.firstName, errorList);
        var patronymicName = UserNameValidator.validated(request.patronymicName, errorList);
        var lastName = UserNameValidator.validated(request.lastName, errorList);

        if (errorList.isEmpty()) {
            return addUser.execute(login.get(), firstName.get(), patronymicName.get(), lastName.get())
                    .fold(
                            (AddUserEndpoint::toRestError),
                            (result -> created(UriComponentsBuilder
                            .fromHttpUrl(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString()
                                    .concat(API_V1_USERS_GET_BY_ID))
                                    .buildAndExpand(result.toLongValue()).toUri()))
            );
        } else {
            return toInvalidParamsBadRequest(errorList);
        }

    }

    /**
     * DTO for AddUserEndpoint
     */
    public record AddUserRestRequest(
            @JsonProperty("login")
            @Schema(description = "User's unique login", example = "vanya.ivanov", required = true) String login,
            @JsonProperty("firstName")
            @Schema(description = "User's name", example = "Vanya", required = true) String firstName,
            @JsonProperty("patronymicName")
            @Schema(description = "User's patronymic", example = "Petrovich", required = true) String patronymicName,
            @JsonProperty("lastName")
            @Schema(description = "User's surname name", example = "Ivanov", required = true) String lastName) {
    }
}
