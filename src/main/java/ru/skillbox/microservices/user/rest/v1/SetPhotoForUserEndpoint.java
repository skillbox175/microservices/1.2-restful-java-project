package ru.skillbox.microservices.user.rest.v1;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.dto.UserModel;
import ru.skillbox.microservices.user.rest.v1.validation.PhotoIdValidator;
import ru.skillbox.microservices.user.rest.v1.validation.UserIdValidator;
import ru.skillbox.microservices.user.usecase.SetPhotoForUser;

import java.util.ArrayList;

import static org.springframework.http.ResponseEntity.ok;
import static ru.skillbox.microservices.user.common.rest.RestResponses.resourceNotFound;
import static ru.skillbox.microservices.user.common.rest.RestResponses.toInvalidParamsBadRequest;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_SET_PHOTO;

@RestController
@Tag(name = "Users")
@AllArgsConstructor
@Getter
public class SetPhotoForUserEndpoint {

    private SetPhotoForUser setPhotoForUser;

    static ResponseEntity<Problem> toRestError(SetPhotoForUser.SetPhotoForUserUseCaseError error) {
        return resourceNotFound();
    }

    /**
     * Executes the set photo for user endpoint.
     *
     * @param userId  The user ID.
     * @param photoId The photo ID.
     * @return The response entity.
     */
    @PostMapping(path = {API_V1_USERS_SET_PHOTO})
    public ResponseEntity<?> execute(@PathVariable long userId, @PathVariable String photoId) {
        var errorList = new ArrayList<ValidationError>();

        var userIdValidated = UserIdValidator.validated(userId, errorList);
        var photoIdValidated = PhotoIdValidator.validated(photoId, errorList);
        if (errorList.isEmpty()) {
            return setPhotoForUser.execute(userIdValidated.get(), photoIdValidated.get())
                    .fold(
                            SetPhotoForUserEndpoint::toRestError,
                            userInfo -> ok(UserModel.from(userInfo))
                    );
        } else {
            return toInvalidParamsBadRequest(errorList);
        }
    }
}