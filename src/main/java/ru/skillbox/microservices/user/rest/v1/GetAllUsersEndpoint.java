package ru.skillbox.microservices.user.rest.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.rest.v1.dto.UserModel;
import ru.skillbox.microservices.user.usecase.GetAllUsers;

import java.util.stream.Stream;

import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_GET_ALL;

@RestController
@AllArgsConstructor
@Getter
public class GetAllUsersEndpoint {

    private GetAllUsers getAllUsers;

    @GetMapping(path = {API_V1_USERS_GET_ALL})
    @Operation(summary = "Get all users", tags = "Users")
    @ApiResponse(
            responseCode = "200",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserModel.class)))
    )
    public ResponseEntity<Stream<UserModel>> execute() {
        return ResponseEntity.ok(getAllUsers.execute().stream().map(UserModel::from));
    }
}
