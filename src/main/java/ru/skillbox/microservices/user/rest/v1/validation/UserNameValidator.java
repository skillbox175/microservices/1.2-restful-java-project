package ru.skillbox.microservices.user.rest.v1.validation;

import io.vavr.control.Either;
import lombok.experimental.UtilityClass;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.domain.UserName;

import java.util.List;

@UtilityClass
public class UserNameValidator {
    public static Either<Boolean, UserName> validated(String value, List<ValidationError> errorList) {
        Either<UserName.CreateUserNameError, UserName> name = UserName.fromString(value);
        var error = (name.mapLeft(it -> {
            if (it instanceof UserName.EmptyUserNameError) {
                return new ValidationError("User name is empty");
            }
            return null;
        }));
        return error.mapLeft(errorList::add);
    }
}
