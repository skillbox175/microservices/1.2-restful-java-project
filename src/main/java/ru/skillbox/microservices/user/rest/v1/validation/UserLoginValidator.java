package ru.skillbox.microservices.user.rest.v1.validation;

import io.vavr.control.Either;
import lombok.experimental.UtilityClass;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.domain.UserLogin;

import java.util.List;

@UtilityClass
public class UserLoginValidator {
    public static Either<Boolean, UserLogin> validated(String value, List<ValidationError> errorList) {
        Either<UserLogin.CreateUserLoginError, UserLogin> login = UserLogin.fromString(value);
        var error = (login.mapLeft(it -> {
            if (it instanceof UserLogin.EmptyUserLoginError) {
                return new ValidationError("User login is empty");
            }
            return null;
        }));
        return error.mapLeft(errorList::add);
    }
}
