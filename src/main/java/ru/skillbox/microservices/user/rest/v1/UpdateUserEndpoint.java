package ru.skillbox.microservices.user.rest.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.dto.UserModel;
import ru.skillbox.microservices.user.rest.v1.validation.UserIdValidator;
import ru.skillbox.microservices.user.rest.v1.validation.UserLoginValidator;
import ru.skillbox.microservices.user.rest.v1.validation.UserNameValidator;
import ru.skillbox.microservices.user.usecase.UpdateUser;

import java.io.Serializable;
import java.util.ArrayList;

import static org.springframework.http.ResponseEntity.ok;
import static ru.skillbox.microservices.user.common.rest.RestResponses.resourceNotFound;
import static ru.skillbox.microservices.user.common.rest.RestResponses.toInvalidParamsBadRequest;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_UPDATE;

@RestController
@Tag(name = "Users")
@AllArgsConstructor
@Getter
public class UpdateUserEndpoint {

    private UpdateUser updateUser;

    static @NonNull ResponseEntity<Problem> toRestError(UpdateUser.UpdateUserUseCaseError error) {
        return resourceNotFound();
    }

    @PutMapping(path = {API_V1_USERS_UPDATE})
    public ResponseEntity<?> execute(@RequestBody UpdateUserRestRequest request) {
        var errorList = new ArrayList<ValidationError>();

        var id = UserIdValidator.validated(request.id, errorList);
        var login = UserLoginValidator.validated(request.login, errorList);
        var firstName = UserNameValidator.validated(request.firstName, errorList);
        var patronymicName = UserNameValidator.validated(request.patronymicName, errorList);
        var lastName = UserNameValidator.validated(request.lastName, errorList);

        if (errorList.isEmpty()) {
            return updateUser.execute(id.get(), login.get(), firstName.get(), patronymicName.get(), lastName.get())
                    .fold(
                            (UpdateUserEndpoint::toRestError),
                            (userInfo -> ok(UserModel.from(userInfo)))

                    );
        } else {
            return toInvalidParamsBadRequest(errorList);
        }
    }

    public record UpdateUserRestRequest(
            @JsonProperty("id") Long id,
            @JsonProperty("login") String login,
            @JsonProperty("firstName") String firstName,
            @JsonProperty("patronymicName") String patronymicName,
            @JsonProperty("lastName") String lastName) implements Serializable {
    }
}
