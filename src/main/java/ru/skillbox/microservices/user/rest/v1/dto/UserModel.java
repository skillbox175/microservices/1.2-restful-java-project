package ru.skillbox.microservices.user.rest.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.skillbox.microservices.user.domain.PhotoId;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public class UserModel {
    @Schema(description = "User's unique id", example = "58432189", required = true)
    Long id;
    @Schema(description = "User's unique login", example = "vanya.ivanov", required = true)
    String login;
    @Schema(description = "User's name", example = "Vanya", required = true)
    String firstName;
    @Schema(description = "User's patronymic name", example = "Petrovich", required = true)
    String patronymicName;
    @Schema(description = "User's surname", example = "Ivanov", required = true)
    String lastName;
    @Schema(description = "Subscribers list")
    Set<UserSimpleModel> subscribers;
    @Schema(description = "Subscription list")
    Set<UserSimpleModel> subscriptions;
    @Schema(description = "Photo Id")
    String photoId;

    public static UserModel from(UserInfo userInfo) {
        var photoId = userInfo.photoId().map(PhotoId::toStringValue).orElse("");

        return new UserModel(
                userInfo.id().toLongValue(),
                userInfo.login().toStringValue(),
                userInfo.firstName().toStringValue(),
                userInfo.patronymicName().toStringValue(),
                userInfo.lastName().toStringValue(),
                userInfo.followings().stream().map(UserSimpleModel::from).collect(Collectors.toSet()),
                userInfo.followers().stream().map(UserSimpleModel::from).collect(Collectors.toSet()),
                photoId
        );
    }
}
