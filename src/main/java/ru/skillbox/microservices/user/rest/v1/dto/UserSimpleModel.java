package ru.skillbox.microservices.user.rest.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.skillbox.microservices.user.usecase.dto.UserSimpleInfo;

@AllArgsConstructor
@Getter
public class UserSimpleModel {
    @Schema(description = "User's unique id", example = "58432189", required = true)
    Long id;
    @Schema(description = "User's unique login", example = "vanya.ivanov", required = true)
    String login;
    @Schema(description = "User's name", example = "Vanya", required = true)
    String firstName;
    @Schema(description = "User's patronymic name", example = "Petrovich", required = true)
    String patronymicName;
    @Schema(description = "User's surname", example = "Ivanov", required = true)
    String lastName;

    public static UserSimpleModel from(UserSimpleInfo user) {
        return new UserSimpleModel(
                user.id().toLongValue(),
                user.login().toStringValue(),
                user.firstName().toStringValue(),
                user.patronymicName().toStringValue(),
                user.lastName().toStringValue()
        );
    }
}
