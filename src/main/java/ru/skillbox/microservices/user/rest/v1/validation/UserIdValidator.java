package ru.skillbox.microservices.user.rest.v1.validation;

import io.vavr.control.Either;
import lombok.experimental.UtilityClass;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.domain.UserId;

import java.util.List;

@UtilityClass
public class UserIdValidator {

    public static Either<Boolean, UserId> validated(Long value, List<ValidationError> errorList) {
        Either<UserId.CreateUserIdError, UserId> userId = UserId.fromLong(value);
        var error = (userId.mapLeft(it -> {
            if (it instanceof UserId.NegativeIdValueError) {
                return new ValidationError("Id has a negative value");
            } else if (it instanceof UserId.EmptyIdValueError) {
                return new ValidationError("Id has an empty value");
            }
            return null;
        }));
        return error.mapLeft(errorList::add);
    }
}