package ru.skillbox.microservices.user.rest.v1;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.microservices.user.common.rest.ValidationError;
import ru.skillbox.microservices.user.rest.v1.dto.UserModel;
import ru.skillbox.microservices.user.rest.v1.validation.UserIdValidator;
import ru.skillbox.microservices.user.usecase.RemovePhotoForUser;

import java.util.ArrayList;

import static org.springframework.http.ResponseEntity.ok;
import static ru.skillbox.microservices.user.common.rest.RestResponses.resourceNotFound;
import static ru.skillbox.microservices.user.common.rest.RestResponses.toInvalidParamsBadRequest;

@RestController
@Tag(name = "Users")
@AllArgsConstructor
@Getter
public class RemovePhotoForUserEndpoint {
    private RemovePhotoForUser removePhotoForUser;

    static ResponseEntity<Problem> toRestError(RemovePhotoForUser.RemovePhotoForUserUseCaseError error) {
        return resourceNotFound();
    }

    /**
     * Executes the remove photo for user endpoint.
     *
     * @param userId The user ID.
     * @return The response entity.
     */
    @DeleteMapping(path = {EndpointUrl.API_V1_USERS_REMOVE_PHOTO})
    public ResponseEntity<?> execute(@PathVariable long userId) {
        var errorList = new ArrayList<ValidationError>();

        var userIdValidated = UserIdValidator.validated(userId, errorList);
        if (errorList.isEmpty()) {
            return removePhotoForUser.execute(userIdValidated.get())
                    .fold(
                            RemovePhotoForUserEndpoint::toRestError,
                            userInfo -> ok(UserModel.from(userInfo))
                    );
        } else {
            return toInvalidParamsBadRequest(errorList);
        }
    }
}
