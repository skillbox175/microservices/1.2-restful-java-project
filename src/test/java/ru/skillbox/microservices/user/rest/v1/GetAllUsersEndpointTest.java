package ru.skillbox.microservices.user.rest.v1;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.microservices.user.rest.v1.testFixtures.MockGetAllUsers;
import ru.skillbox.microservices.user.usecase.GetAllUsers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_GET_ALL;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.UrlBuilder;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.userInfo;

@WebMvcTest
@ContextConfiguration(classes = GetAllUsersEndpointTest.TestConfiguration.class)
class GetAllUsersEndpointTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    MockGetAllUsers getAllUsers;

    @Test
    @SneakyThrows
    void get_all_users() {
        var userInfo = getAllUsers.userInfo;

        var url = new UrlBuilder(API_V1_USERS_GET_ALL).withHost().build();
        mockMvc.perform(get(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_JSON),
                        status().isOk(),
                        jsonPath("$.length()").value(1),
                        jsonPath("$[0].id").value(userInfo.id().toLongValue()),
                        jsonPath("$[0].login").value(userInfo.login().toStringValue()),
                        jsonPath("$[0].firstName").value(userInfo.firstName().toStringValue()),
                        jsonPath("$[0].patronymicName").value(userInfo.patronymicName().toStringValue()),
                        jsonPath("$[0].lastName").value(userInfo.lastName().toStringValue())
                );
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public MockGetAllUsers getAllUsers() {
            return new MockGetAllUsers(userInfo());
        }

        @Bean
        public GetAllUsersEndpoint getAllUsersEndpoint(GetAllUsers getAllUsers) {
            return new GetAllUsersEndpoint(getAllUsers);
        }
    }
}
