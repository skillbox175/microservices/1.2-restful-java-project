package ru.skillbox.microservices.user.rest.v1;

import io.vavr.control.Either;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.rest.v1.testFixtures.MockUnfollowUser;
import ru.skillbox.microservices.user.usecase.UnfollowUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_UNFOLLOW_BY_ID;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.*;

@WebMvcTest
@ContextConfiguration(classes = UnfollowUserEndpointTest.TestConfiguration.class)
class UnfollowUserEndpointTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    MockUnfollowUser mockUnfollowUser;

    @Test
    @SneakyThrows
    void validation_error() {
        String url =
                new UrlBuilder(API_V1_USERS_UNFOLLOW_BY_ID).withHost().build()
                        .replace("{followingId}", "-1").replace("{followerId}", "0");
        mockMvc.perform(post(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isBadRequest(),
                        jsonPath("$.type").value(badRequestTypeUrl()),
                        jsonPath("$.status").value(HttpStatus.BAD_REQUEST.value()),
                        jsonPath("$.invalid_params.length()").value(2));
    }

    @Test
    @SneakyThrows
    void successfully_unfollowed() {
        UserId followingId = userId();
        UserId currUserId = userId();
        mockUnfollowUser.response = Either.right(null);

        String url =
                new UrlBuilder(API_V1_USERS_UNFOLLOW_BY_ID).withHost().build()
                        .replace("{followingId}", followingId.toLongValue().toString())
                        .replace("{followerId}", currUserId.toLongValue().toString());
        mockMvc.perform(post(url))
                .andExpectAll(
                        content().string(""),
                        status().isOk()
                );

        mockUnfollowUser.verifyInvoked(currUserId);
    }

    @Test
    @SneakyThrows
    void requested_user_is_not_following() {
        UserId followingId = userId();
        UserId currUserId = userId();
        mockUnfollowUser.response = Either.left(new UnfollowUser.NotFollowing());

        String url = new UrlBuilder(API_V1_USERS_UNFOLLOW_BY_ID).withHost().build()
                .replace("{followingId}", followingId.toLongValue().toString())
                .replace("{followerId}", currUserId.toLongValue().toString());

        mockMvc.perform(post(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isUnprocessableEntity(),
                        jsonPath("$.type").value(errorTypeUrl("not_following")),
                        jsonPath("$.status").value(HttpStatus.UNPROCESSABLE_ENTITY.value())
                );
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public UnfollowUserEndpoint unfollowUserEndpoint(UnfollowUser unfollowUser) {
            return new UnfollowUserEndpoint(unfollowUser);
        }

        @Bean
        public MockUnfollowUser unfollowUser() {
            return new MockUnfollowUser();
        }
    }
}
