package ru.skillbox.microservices.user.rest.v1;


import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.microservices.user.usecase.RemovePhotoForUser;
import ru.skillbox.microservices.user.usecase.scenario.RemovePhotoForUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.photoId;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;
import static ru.skillbox.microservices.user.persistence.postgres.Fixtures.newUserWithPhoto;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_REMOVE_PHOTO;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.badRequestTypeUrl;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.notFoundTypeUrl;

@WebMvcTest
@ContextConfiguration(classes = RemovePhotoForUserEndpointTest.TestConfiguration.class)
class RemovePhotoForUserEndpointTest {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    MockUserExtractor userExtractor;
    @Autowired
    MockUserPersister userPersister;

    @Test
    @SneakyThrows
    void validation_error() {
        var userId = -1L;

        mockMvc.perform(delete(API_V1_USERS_REMOVE_PHOTO, userId))
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type", Matchers.equalTo(badRequestTypeUrl())))
                .andExpect(jsonPath("$.status", Matchers.equalTo(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath("$.invalid_params.length()", Matchers.equalTo(1)));
    }

    @Test
    @SneakyThrows
    void user_not_found() {

        var userId = userId().toLongValue();

        mockMvc.perform(delete(API_V1_USERS_REMOVE_PHOTO, userId))
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", Matchers.equalTo(notFoundTypeUrl())))
                .andExpect(jsonPath("$.status", Matchers.equalTo(HttpStatus.NOT_FOUND.value())));
    }

    @Test
    @SneakyThrows
    void removePhoto_successfully() {
        var user = newUserWithPhoto();
        userExtractor.user = user;

        var userId = user.id.toLongValue();
        var photoId = photoId().toStringValue();


        mockMvc.perform(delete(API_V1_USERS_REMOVE_PHOTO, userId, photoId))
                .andExpectAll(
                        jsonPath("$.id").value(user.id.toLongValue()),
                        jsonPath("$.login").value(user.userLogin.toStringValue()),
                        jsonPath("$.firstName").value(user.firstName.toStringValue()),
                        jsonPath("$.patronymicName").value(user.patronymicName.toStringValue()),
                        jsonPath("$.lastName").value(user.lastName.toStringValue()),
                        jsonPath("$.photoId").value(""),
                        status().isOk()
                );
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public RemovePhotoForUserEndpoint addUserEndpoint(
                RemovePhotoForUser removePhotoForUser) {
            return new RemovePhotoForUserEndpoint(removePhotoForUser);
        }

        @Bean
        public RemovePhotoForUserUseCase removePhotoForUser(MockUserExtractor userExtractor, MockUserPersister userPersister) {
            return new RemovePhotoForUserUseCase(userExtractor, userPersister);
        }

        @Bean
        public MockUserExtractor userExtractor() {
            return new MockUserExtractor();
        }

        @Bean
        MockUserPersister userPersister() {
            return new MockUserPersister();
        }
    }
}
