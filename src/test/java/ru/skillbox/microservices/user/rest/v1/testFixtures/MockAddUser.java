package ru.skillbox.microservices.user.rest.v1.testFixtures;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;
import ru.skillbox.microservices.user.usecase.AddUser;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AllArgsConstructor
@NoArgsConstructor
public class MockAddUser implements AddUser {

    public Either<AddUserUseCaseError, UserId> response;
    public UserLogin login;
    public UserName firstName;
    public UserName patronymicName;
    public UserName lastName;

    @Override
    public Either<AddUserUseCaseError, UserId> execute(
            UserLogin login,
            UserName firstName,
            UserName patronymicName,
            UserName lastName
    ) {
        this.login = login;
        this.firstName = firstName;
        this.patronymicName = patronymicName;
        this.lastName = lastName;
        return response;
    }

    public void verifyInvoked(
            UserLogin login,
            UserName firstName,
            UserName patronymicName,
            UserName lastName
            ) {
        assertEquals(login, this.login);
        assertEquals(firstName, this.firstName);
        assertEquals(patronymicName, this.patronymicName);
        assertEquals(lastName, this.lastName);
    }
}
