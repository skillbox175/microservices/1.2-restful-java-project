package ru.skillbox.microservices.user.rest.v1;

import io.vavr.control.Either;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.microservices.user.rest.v1.testFixtures.MockRemoveUser;
import ru.skillbox.microservices.user.usecase.RemoveUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_DELETE_BY_ID;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.*;

@WebMvcTest
@ContextConfiguration(classes = {RemoveUserEndpointTest.TestConfiguration.class})
class RemoveUserEndpointTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    MockRemoveUser removeUser;

    @Test
    @SneakyThrows
    void validation_error() {
        removeUser.response = Either.right(null);

        String url = new UrlBuilder(API_V1_USERS_DELETE_BY_ID).withId(-1 * userId().toLongValue()).withHost().build();
        mockMvc.perform(delete(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isBadRequest(),
                        jsonPath("$.type", Matchers.equalTo(badRequestTypeUrl())),
                        jsonPath("$.status", Matchers.equalTo(HttpStatus.BAD_REQUEST.value())),
                        jsonPath("$.invalid_params.length()", Matchers.equalTo(1))
                );
    }

    @Test
    @SneakyThrows
    void user_not_found() {
        removeUser.response = Either.left(new RemoveUser.UserNotFound());

        String url = new UrlBuilder(API_V1_USERS_DELETE_BY_ID).withId(userId().toLongValue()).withHost().build();
        mockMvc.perform(delete(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isNotFound(),
                        jsonPath("$.type", Matchers.equalTo(notFoundTypeUrl())),
                        jsonPath("$.status", Matchers.equalTo(HttpStatus.NOT_FOUND.value()))
                );
    }

    @Test
    @SneakyThrows
    void remove_successfully() {
        removeUser.response = Either.right(null);

        String url = new UrlBuilder(API_V1_USERS_DELETE_BY_ID).withId(userId().toLongValue()).withHost().build();
        mockMvc.perform(delete(url))
                .andExpect(content().string(""))
                .andExpect(status().isNoContent());
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public RemoveUserEndpoint removeUserEndpoint(
                @Qualifier("mockRemoveUser")
                RemoveUser removeUser) {
            return new RemoveUserEndpoint(removeUser);
        }

        @Bean
        public MockRemoveUser mockRemoveUser() {
            return new MockRemoveUser();
        }
    }
}
