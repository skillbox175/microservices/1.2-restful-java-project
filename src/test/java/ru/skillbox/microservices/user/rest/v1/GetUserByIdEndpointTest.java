package ru.skillbox.microservices.user.rest.v1;

import io.vavr.control.Either;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.UrlBuilder;
import ru.skillbox.microservices.user.rest.v1.testFixtures.MockGetUserById;
import ru.skillbox.microservices.user.usecase.GetUserById;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_GET_BY_ID;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.notFoundTypeUrl;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.userInfo;

@WebMvcTest
@ContextConfiguration(classes = GetUserByIdEndpointTest.TestConfiguration.class)
class GetUserByIdEndpointTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    MockGetUserById getUserById;

    @Test
    @SneakyThrows
    void user_not_found() {
        getUserById.response = Either.left(new GetUserById.UserNotFound());

        String url = new UrlBuilder(API_V1_USERS_GET_BY_ID)
                .withId(userId().toLongValue()).withHost().build();
        mockMvc.perform(get(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isNotFound(),
                        jsonPath("$.type").value(notFoundTypeUrl()),
                        jsonPath("$.status").value(HttpStatus.NOT_FOUND.value())
                );
    }

    @Test
    @SneakyThrows
    void returned_successfully() {
        var userInfo = userInfo();
        getUserById.response = Either.right(userInfo);

        String url = new UrlBuilder(API_V1_USERS_GET_BY_ID)
                .withId(userInfo.id().toLongValue()).withHost().build();
        mockMvc.perform(get(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_JSON),
                        jsonPath("$.id").value(userInfo.id().toLongValue()),
                        jsonPath("$.login").value(userInfo.login().toStringValue()),
                        jsonPath("$.firstName").value(userInfo.firstName().toStringValue()),
                        jsonPath("$.patronymicName").value(userInfo.patronymicName().toStringValue()),
                        jsonPath("$.lastName").value(userInfo.lastName().toStringValue())
                );

        getUserById.verifyInvoked(userInfo.id());
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public MockGetUserById getUserById() {
            return new MockGetUserById();
        }

        @Bean
        public GetUserByIdEndpoint getUserByIdEndpoint(GetUserById getUserById) {
            return new GetUserByIdEndpoint(getUserById);
        }
    }

}
