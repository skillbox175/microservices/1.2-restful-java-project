package ru.skillbox.microservices.user.rest.v1;

import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.UpdateUser;
import ru.skillbox.microservices.user.usecase.scenario.UpdateUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_UPDATE;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.badRequestTypeUrl;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.notFoundTypeUrl;

@WebMvcTest
@ContextConfiguration(classes = UpdateUserEndpointTest.TestConfiguration.class)
class UpdateUserEndpointTest {
    private final ObjectMapper mapper = new ObjectMapper();
    @Autowired
    MockMvc mockMvc;
    @Autowired
    MockUserExtractor userExtractor;
    @Autowired
    MockUserPersister userPersister;

    @Test
    @SneakyThrows
    void validation_error() {
        mockMvc.perform(put(API_V1_USERS_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                mapper.writeValueAsString(
                                        new UpdateUserEndpoint.UpdateUserRestRequest(
                                                -1L,
                                                "",
                                                "",
                                                "",
                                                ""
                                        )
                                )
                        )
                ).andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type", Matchers.equalTo(badRequestTypeUrl())))
                .andExpect(jsonPath("$.status", Matchers.equalTo(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath("$.invalid_params.length()", Matchers.equalTo(5)));
    }

    @Test
    @SneakyThrows
    void user_not_found() {

        UserEntity user = user();

        mockMvc.perform(put(API_V1_USERS_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                mapper.writeValueAsString(
                                        new UpdateUserEndpoint.UpdateUserRestRequest(
                                                user.id.toLongValue(),
                                                user.userLogin.toStringValue(),
                                                user.firstName.toStringValue(),
                                                user.patronymicName.toStringValue(),
                                                user.lastName.toStringValue()
                                        )
                                )
                        )
                ).andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", Matchers.equalTo(notFoundTypeUrl())))
                .andExpect(jsonPath("$.status", Matchers.equalTo(HttpStatus.NOT_FOUND.value())));
    }

    @Test
    @SneakyThrows
    void updated_successfully() {
        UserId userId = userId();
        UserEntity user = user(userId);

        userExtractor.user = user;

        mockMvc.perform(put(API_V1_USERS_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                mapper.writeValueAsString(
                                        new UpdateUserEndpoint.UpdateUserRestRequest(
                                                user.id.toLongValue(),
                                                user.userLogin.toStringValue(),
                                                user.firstName.toStringValue(),
                                                user.patronymicName.toStringValue(),
                                                user.lastName.toStringValue()
                                        )
                                )
                        )
        ).andExpectAll(
                jsonPath("$.id").value(user.id.toLongValue()),
                jsonPath("$.login").value(user.userLogin.toStringValue()),
                jsonPath("$.firstName").value(user.firstName.toStringValue()),
                jsonPath("$.patronymicName").value(user.patronymicName.toStringValue()),
                jsonPath("$.lastName").value(user.lastName.toStringValue()),
                status().isOk()
        );
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public UpdateUserEndpoint updateUserEndpoint(
                UpdateUser updateUser) {
            return new UpdateUserEndpoint(updateUser);
        }

        @Bean
        public UpdateUserUseCase updateUser(MockUserPersister userPersister, MockUserExtractor userExtractor) {
            return new UpdateUserUseCase(userPersister, userExtractor);
        }

        @Bean
        public MockUserExtractor userExtractor() {
            return new MockUserExtractor();
        }

        @Bean
        MockUserPersister userPersister() {
            return new MockUserPersister();
        }
    }
}
