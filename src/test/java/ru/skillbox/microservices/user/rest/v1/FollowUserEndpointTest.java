package ru.skillbox.microservices.user.rest.v1;

import io.vavr.control.Either;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures;
import ru.skillbox.microservices.user.rest.v1.testFixtures.MockFollowUser;
import ru.skillbox.microservices.user.usecase.FollowUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_FOLLOW_BY_ID;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.*;

@WebMvcTest
@ContextConfiguration(classes = FollowUserEndpointTest.TestConfiguration.class)
class FollowUserEndpointTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    MockFollowUser mockFollowUser;

    @Test
    @SneakyThrows
    void validation_error() {
        String url = new Fixtures.UrlBuilder(API_V1_USERS_FOLLOW_BY_ID).withHost().build()
                .replace("{followingId}", "-1").replace("{followerId}", "0");
        mockMvc.perform(post(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isBadRequest(),
                        jsonPath("$.type").value(badRequestTypeUrl()),
                        jsonPath("$.status").value(HttpStatus.BAD_REQUEST.value()),
                        jsonPath("$.invalid_params.length()").value(2));
    }

    @Test
    @SneakyThrows
    void successfully_followed() {
        UserId followingId = userId();
        UserId currUserId = userId();
        mockFollowUser.response = Either.right(null);

        String url = new Fixtures.UrlBuilder(API_V1_USERS_FOLLOW_BY_ID).withHost().build()
                .replace("{followingId}", followingId.toLongValue().toString())
                .replace("{followerId}", currUserId.toLongValue().toString());
        mockMvc.perform(post(url))
                .andExpectAll(
                        content().string(""),
                        status().isOk()
                );

        mockFollowUser.verifyInvoked(followingId,currUserId);
    }

    @Test
    @SneakyThrows
    void already_subscribed() {
        UserId followingId = userId();
        UserId currUserId = userId();
        mockFollowUser.response = Either.left(new FollowUser.UserAlreadySubscribed());

        String url = new Fixtures.UrlBuilder(API_V1_USERS_FOLLOW_BY_ID).withHost().build()
                .replace("{followingId}", followingId.toLongValue().toString())
                .replace("{followerId}", currUserId.toLongValue().toString());
        mockMvc.perform(post(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isUnprocessableEntity(),
                        jsonPath("$.type").value(errorTypeUrl("already_subscribed")),
                        jsonPath("$.status").value(HttpStatus.UNPROCESSABLE_ENTITY.value())
                );
    }

    @Test
    @SneakyThrows
    void subscription_not_exist() {
        UserId followingId = userId();
        UserId currUserId = userId();
        mockFollowUser.response = Either.left(new FollowUser.FollowedNotFound());

        String url = new Fixtures.UrlBuilder(API_V1_USERS_FOLLOW_BY_ID).withHost().build()
                .replace("{followingId}", followingId.toLongValue().toString())
                .replace("{followerId}", currUserId.toLongValue().toString());
        mockMvc.perform(post(url))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isNotFound(),
                        jsonPath("$.type").value(notFoundTypeUrl()),
                        jsonPath("$.status").value(HttpStatus.NOT_FOUND.value())
                );
    }
    @Configuration
    static class TestConfiguration {
        @Bean
        public FollowUserEndpoint followUserEndpoint(FollowUser followUser) {
            return new FollowUserEndpoint(followUser);
        }

        @Bean
        public MockFollowUser followUser() {
            return new MockFollowUser();
        }
    }
}
