package ru.skillbox.microservices.user.rest.v1.testFixtures;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.FollowUser;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;

@AllArgsConstructor
@NoArgsConstructor
public class MockFollowUser implements FollowUser {
    public Either<FollowUser.FollowUserUseCaseError, Void> response;
    public UserEntity currUser;
    public MockUserPersister userPersister = new MockUserPersister();

    @Override
    public Either<FollowUser.FollowUserUseCaseError, Void> execute(UserId currUserId, UserId followingId) {
        this.currUser = user(currUserId);
        userPersister.subscription.put(followingId, currUserId);
        return response;
    }

    public void verifyInvoked(UserId followingId, UserId currUserId) {
        assertTrue(this.userPersister.subscription.containsKey(followingId));
        assertEquals(currUserId, this.currUser.id);
    }
}
