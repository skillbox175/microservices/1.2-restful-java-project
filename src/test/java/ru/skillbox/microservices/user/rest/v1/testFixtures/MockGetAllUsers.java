package ru.skillbox.microservices.user.rest.v1.testFixtures;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.usecase.GetAllUsers;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class MockGetAllUsers implements GetAllUsers {
    public UserInfo userInfo;

    @Override
    public List<UserInfo> execute() {
        return List.of(userInfo);
    }
}
