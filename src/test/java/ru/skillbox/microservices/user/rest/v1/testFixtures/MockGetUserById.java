package ru.skillbox.microservices.user.rest.v1.testFixtures;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.GetUserById;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AllArgsConstructor
@NoArgsConstructor
public class MockGetUserById implements GetUserById {
    public Either<GetUserByIdUseCaseError, UserInfo> response;
    UserId id;


    @Override
    public Either<GetUserByIdUseCaseError, UserInfo> execute(UserId id) {
        this.id = id;
        return response;
    }

    public void verifyInvoked(UserId id) {
        assertEquals(this.id, id);
    }
}
