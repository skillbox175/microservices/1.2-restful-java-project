package ru.skillbox.microservices.user.rest.v1.testFixtures;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.UnfollowUser;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;

@AllArgsConstructor
@NoArgsConstructor
public class MockUnfollowUser implements UnfollowUser {
    public Either<UnfollowUser.UnfollowUserUseCaseError, Void> response;
    public UserEntity currUser;
    public MockUserPersister userPersister = new MockUserPersister();

    @Override
    public Either<UnfollowUserUseCaseError, Void> execute(UserId currUserId, UserId followingId) {
        this.currUser = user(currUserId);
        this.userPersister.subscription.remove(followingId);
        return response;
    }

    public void verifyInvoked(UserId currUserId) {
        assertTrue(this.userPersister.subscription.isEmpty());
        assertEquals(currUserId, this.currUser.id);
    }
}
