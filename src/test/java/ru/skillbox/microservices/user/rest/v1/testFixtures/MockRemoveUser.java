package ru.skillbox.microservices.user.rest.v1.testFixtures;

import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.RemoveUser;

import static org.junit.jupiter.api.Assertions.assertEquals;


@AllArgsConstructor
@NoArgsConstructor
public class MockRemoveUser implements RemoveUser{
    public Either<RemoveUser.RemoveUserUseCaseError, Void> response;
    public UserId id;


    @Override
    public Either<RemoveUserUseCaseError, Void> execute(UserId id) {
        this.id = id;
        return response;
    }

    public void verifyInvoked(UserId id) {
        assertEquals(this.id, id);
    }
}
