package ru.skillbox.microservices.user.rest.v1;

import io.vavr.control.Either;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;
import ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.UrlBuilder;
import ru.skillbox.microservices.user.rest.v1.testFixtures.MockAddUser;
import ru.skillbox.microservices.user.usecase.AddUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_ADD;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_GET_BY_ID;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.badRequestTypeUrl;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.errorTypeUrl;

@WebMvcTest
@ContextConfiguration(classes = AddUserEndpointTest.TestConfiguration.class)
class AddUserEndpointTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    MockAddUser mockAddUser;
    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    @SneakyThrows
    void validation_error() {
        mockMvc.perform(post(API_V1_USERS_ADD)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                mapper.writeValueAsString(
                                        new AddUserEndpoint.AddUserRestRequest(
                                                "",
                                                "",
                                                "",
                                                ""
                                        )
                                )
                        )
        ).andExpectAll(content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                status().isBadRequest(),
                jsonPath("$.type").value(badRequestTypeUrl()),
                jsonPath("$.status").value(HttpStatus.BAD_REQUEST.value()),
                jsonPath("$.invalid_params.length()").value(4));
    }

    @Test
    @SneakyThrows
    void user_already_exists() {
        mockAddUser.response = Either.left(new AddUser.AlreadyExists());

        UserName firstName = firstName();
        UserName patronymicName = patronymicName();
        UserName lastName = lastName();
        UserLogin login = login(firstName.toStringValue(), lastName.toStringValue());

        mockMvc.perform(post(API_V1_USERS_ADD)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        mapper.writeValueAsString(
                                new AddUserEndpoint.AddUserRestRequest(
                                        login.toStringValue(),
                                        firstName.toStringValue(),
                                        patronymicName.toStringValue(),
                                        lastName.toStringValue()
                                )
                        )
                )
        ).andExpectAll(
                content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                status().isUnprocessableEntity(),
                jsonPath("$.type").value(errorTypeUrl("already_exists")),
                jsonPath("$.status").value(HttpStatus.UNPROCESSABLE_ENTITY.value())
        );

        mockAddUser.verifyInvoked(login, firstName, patronymicName, lastName);
    }

    @Test
    @SneakyThrows
    void created_successfully() {
        UserId userId = userId();
        mockAddUser.response = Either.right(userId);

        UserName firstName = firstName();
        UserName patronymicName = patronymicName();
        UserName lastName = lastName();
        UserLogin login = login(firstName.toStringValue(), lastName.toStringValue());

        var url = new UrlBuilder(API_V1_USERS_ADD).withHost().build();

        mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        mapper.writeValueAsString(
                                new AddUserEndpoint.AddUserRestRequest(
                                        login.toStringValue(),
                                        firstName.toStringValue(),
                                        patronymicName.toStringValue(),
                                        lastName.toStringValue()
                                )
                        )
                )
        ).andExpectAll(
                content().string(""),
                status().isCreated(),
                header().string(
                        "Location",
                        (new UrlBuilder(API_V1_USERS_GET_BY_ID)
                                .withId(userId.toLongValue()).withHost().build()
                        )
                )
        );
        mockAddUser.verifyInvoked(login, firstName, patronymicName, lastName);
    }

    @Configuration
    static class TestConfiguration {

        @Bean
        public AddUserEndpoint addUserEndpoint(
                AddUser addUser) {
            return new AddUserEndpoint(addUser);
        }

        @Bean
        public MockAddUser addUser() {
            return new MockAddUser();
        }
    }

}
