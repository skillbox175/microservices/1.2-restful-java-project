package ru.skillbox.microservices.user.rest.v1.testFixtures;

import lombok.NonNull;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;

import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;

public class Fixtures {
    static final String API_V1_TYPE_BASE_URL = "http://localhost";

    public static String errorTypeUrl(String suffix) {
        return String.format("%s/%s", API_V1_TYPE_BASE_URL, suffix);
    }

    public static String notFoundTypeUrl() {
        return errorTypeUrl("resource_not_found");
    }

    public static String badRequestTypeUrl() {
        return errorTypeUrl("bad_request");
    }

    public record UrlBuilder(String url) {
        public UrlBuilder withId(@NonNull Long id) {
            return new UrlBuilder(this.url().replace("{id}", id.toString()));
        }

        public UrlBuilder withHost() {
            return new UrlBuilder(API_V1_TYPE_BASE_URL + this.url());
        }

        public String build() {
            return this.url();
        }

    }

    public static UserInfo userInfo() {
        var user = user();
        return UserInfo.from(user);
    }

}
