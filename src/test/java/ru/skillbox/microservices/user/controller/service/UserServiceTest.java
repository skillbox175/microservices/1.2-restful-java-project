package ru.skillbox.microservices.user.controller.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.microservices.user.legacy.entity.User;
import ru.skillbox.microservices.user.legacy.repository.LegacyUserRepository;
import ru.skillbox.microservices.user.legacy.service.UserService;

import javax.persistence.PersistenceException;
import java.util.Optional;

class UserServiceTest {

    @Test
    void createUser_Successful() {
        //given
        LegacyUserRepository userRepository = Mockito.mock(LegacyUserRepository.class);
        User user = new User("vasya.petrov", "Vasya", "Petrov", "Ivanovich");
        User savedUser = new User(1L, "vasya.petrov", "Vasya", "Petrov", "Ivanovich");
        Mockito.when(userRepository.save(user)).thenReturn(savedUser);
        UserService userService = new UserService(userRepository);

        //when
        String result = userService.createUser(user);

        //then
        Assertions.assertEquals("Пользователь vasya.petrov добавлен в базу с id = 1", result);
        Assertions.assertEquals(savedUser, user);
    }

    @Test
    void createUser_Error() {
        //given
        LegacyUserRepository userRepository = Mockito.mock(LegacyUserRepository.class);
        User user = new User("vasya.petrov", "Vasya", "Petrov", "Ivanovich");
        Mockito.when(userRepository.save(user)).thenThrow(PersistenceException.class);
        UserService userService = new UserService(userRepository);

        //when
        Executable executable = () -> userService.createUser(user);

        //then
        Assertions.assertThrows(PersistenceException.class, executable);
    }

    @Test
    void getUser_Successful() {
        //given
        LegacyUserRepository userRepository = Mockito.mock(LegacyUserRepository.class);

        User user = new User(1L, "vasya.petrov", "Vasya", "Petrov", "Ivanovich");
        Long userId = user.getId();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        UserService userService = new UserService(userRepository);

        //when
        String result = userService.getUser(userId).toString();

        //then
        Assertions.assertEquals("User{id=1, login='vasya.petrov', firstName='Vasya', lastName='Petrov', patronymicName='Ivanovich'}", result);
    }

    @Test
    void UpdateUser_Successful() {
        //given
        LegacyUserRepository userRepository = Mockito.mock(LegacyUserRepository.class);
        User userForUpdate = new User("vasya.petrov", "Vasya", "Ivanov", "Ivanovich");
        User updatedUser = new User(1L, "vasya.petrov", "Vasya", "Ivanov", "Ivanovich");
        Mockito.when(userRepository.save(userForUpdate)).thenReturn(updatedUser);
        Mockito.when(userRepository.existsById(1L)).thenReturn(Boolean.TRUE);
        UserService userService = new UserService(userRepository);

        //when
        String result = userService.updateUser(userForUpdate, 1L);

        //then
        Assertions.assertEquals("Пользователь vasya.petrov успешно сохранен", result);
    }

    @Test
    void UpdateUser_Error() {
        //given
        LegacyUserRepository userRepository = Mockito.mock(LegacyUserRepository.class);

        User userForUpdate = new User("vasya.petrov", "Vasya", "Ivanov", "Ivanovich");
        User updatedUser = new User(1L, "vasya.petrov", "Vasya", "Ivanov", "Ivanovich");
        Mockito.when(userRepository.save(userForUpdate)).thenReturn(updatedUser);
        Mockito.when(userRepository.existsById(1L)).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND));
        UserService userService = new UserService(userRepository);

        //when
        Executable executable = () -> userService.updateUser(userForUpdate, 1L);

        //then
        Assertions.assertThrows(ResponseStatusException.class, executable);
    }
}