package ru.skillbox.microservices.user.application.event;

import lombok.Getter;
import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.application.event.Fixtures.AnotherTestEvent;
import ru.skillbox.microservices.user.application.event.Fixtures.TestEvent;
import ru.skillbox.microservices.user.common.events.DomainEventListener;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EventPublisherImplTest {

    @Test
    void publish_events() {
        EventPublisherImpl publisher = new EventPublisherImpl();

        TestEventListener testEventListener = new TestEventListener();
        publisher.registerListener(testEventListener);

        AnotherTestEventListener anotherTestEventListener = new AnotherTestEventListener();
        publisher.registerListener(anotherTestEventListener);

        TestEvent testEvent = new TestEvent("TestEvent");
        AnotherTestEvent anotherTestEvent = new AnotherTestEvent("AnotherTestEvent");
        List<DomainEvent> events = Arrays.asList(testEvent, anotherTestEvent);

        publisher.publish(events);

        assertEquals(List.of(testEvent), testEventListener.getEvents());
        assertEquals(List.of(anotherTestEvent), anotherTestEventListener.getEvents());
    }

    @Getter
    static class TestEventListener implements DomainEventListener<TestEvent> {
        List<TestEvent> events = new ArrayList<>();

        @Override
        public Class<?> eventType() {
            return TestEvent.class;
        }

        @Override
        public void handle(DomainEvent event) {
            events.add((TestEvent) event);
        }
    }

    @Getter
    static class AnotherTestEventListener implements DomainEventListener<AnotherTestEvent> {
        List<AnotherTestEvent> events = new ArrayList<>();

        @Override
        public Class<?> eventType() {
            return AnotherTestEvent.class;
        }

        @Override
        public void handle(DomainEvent event) {
            events.add((AnotherTestEvent) event);
        }
    }

}
