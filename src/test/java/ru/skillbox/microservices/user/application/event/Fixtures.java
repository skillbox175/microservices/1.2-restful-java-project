package ru.skillbox.microservices.user.application.event;

import lombok.ToString;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;

public class Fixtures {
    @ToString
    static class TestEvent extends DomainEvent {
        public final String name;

        public TestEvent(String name) {
            super();
            this.name = name;
        }
    }

    @ToString
    static class AnotherTestEvent extends DomainEvent {
        public final String name;

        public AnotherTestEvent(String name) {
            super();
            this.name = name;
        }
    }
}
