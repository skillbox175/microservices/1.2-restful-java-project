package ru.skillbox.microservices.user.application.event;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.testcontainers.kafka.KafkaContainer;
import ru.skillbox.microservices.user.application.event.Fixtures.TestEvent;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@SpringBootTest(classes = KafkaEventPublisherTest.TestConfiguration.class)
class KafkaEventPublisherTest {

    @Autowired
    private KafkaEventPublisher publisher;

    @Autowired
    private KafkaConsumer subscriber;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Test
    void publish_validEvent_sendsEventToKafka() {
        DomainEvent testEvent = new TestEvent("TestEvent1");
        DomainEvent anotherTestEvent = new TestEvent("TestEvent2");
        var eventList = List.of(testEvent, anotherTestEvent);
        publisher.publish(eventList);
        subscriber.subscribe(Collections.singleton("userEvents"));
        var events = subscriber.poll(1000);
        assertEquals(2, events.count());

        events.records("userEvents").forEach(record -> {

            ObjectMapper mapper = new ObjectMapper();
            var json = ((ConsumerRecord<String, String>) record).value();
            TestEvent event = null;

            try {
                event = mapper.readValue(json, TestEvent.class);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            System.out.println(event);
        });
    }

    @Test
    void createUser_event_successfully_published() {

    }

    @Configuration
    static class TestConfiguration {
        @Bean(initMethod = "start")
        public KafkaContainer kafkaContainer() {
            return new KafkaContainer("apache/kafka-native:3.8.0");
        }

        @Bean
        public Map<String, Object> consumerConfigs(KafkaContainer kafkaContainer) {
            Map<String, Object> props = new HashMap<>();
            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaContainer.getBootstrapServers());
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            props.put(ConsumerConfig.GROUP_ID_CONFIG, "test-group");
            // more standard configuration
            return props;
        }

        @Bean
        public ProducerFactory<String, String> producerFactory(KafkaContainer kafkaContainer) {
            Map<String, Object> configProps = new HashMap<>();
            configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaContainer.getBootstrapServers());
            configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
            configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
            // more standard configuration
            return new DefaultKafkaProducerFactory<>(configProps);
        }

        @Bean
        public KafkaTemplate<String, String> kafkaTemplate(ProducerFactory<String, String> producerFactory) {
            return new KafkaTemplate<>(producerFactory);
        }

        @Bean
        public KafkaEventPublisher publisher(KafkaTemplate<String, String> kafkaTemplate) {
            return new KafkaEventPublisher(kafkaTemplate);
        }

        @Bean
        public KafkaConsumer<String, String> subscriber(KafkaContainer kafkaContainer) {
            Map<String, Object> props = new HashMap<>();
            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaContainer.getBootstrapServers());
            props.put(ConsumerConfig.GROUP_ID_CONFIG, "test-group");
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

            KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
            consumer.subscribe(Collections.singletonList("userEvents"));

            return consumer;
        }
    }
}
