package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserIdTest {

    @Test
    void check_equality() {
        Random random = new Random();
        Long id = Math.abs(random.nextLong());

        UserId userId1 = UserId.fromLong(id).get();
        UserId userId2 = UserId.fromLong(id).get();

        assertEquals(userId1, userId2);
        assertEquals(userId1.getClass(), userId2.getClass());
    }

    @Test
    void negative_user_id_error() {
        Random random = new Random();
        Long id = -1 * Math.abs(random.nextLong());
        Either<UserId.CreateUserIdError, UserId> result = UserId.fromLong(id);
        assertTrue(result.isLeft());
    }
}
