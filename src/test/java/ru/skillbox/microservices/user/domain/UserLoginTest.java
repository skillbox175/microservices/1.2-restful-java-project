package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.firstName;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.lastName;

class UserLoginTest {
    @Test
    void createUserLogin_Success() {
        String value = firstName().toStringValue().toLowerCase() + "." + lastName().toStringValue().toLowerCase();
        Either<UserLogin.CreateUserLoginError, UserLogin> result = UserLogin.fromString(value);

        assertEquals(result.get().getClass(), UserLogin.class);
        assertEquals(result.get().toStringValue(), value);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    void createUserLogin_EmptyValueException(String input) {
        Either<UserLogin.CreateUserLoginError, UserLogin> login = UserLogin.fromString(input);
        assertTrue( login.isLeft());
    }

    @Test
    void createUserLogin_WrongFormatException() {
        String value = firstName().toStringValue();
        Either<UserLogin.CreateUserLoginError, UserLogin> result = UserLogin.fromString(value);

        assertTrue(result.isLeft());
        assertEquals(result.getLeft().getClass(), UserLogin.WrongUserLoginFormatError.class);
    }

}
