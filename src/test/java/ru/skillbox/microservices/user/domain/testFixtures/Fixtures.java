package ru.skillbox.microservices.user.domain.testFixtures;

import com.github.javafaker.Faker;
import io.vavr.control.Either;
import ru.skillbox.microservices.user.common.types.base.Version;
import ru.skillbox.microservices.user.domain.*;

import java.util.Optional;
import java.util.Random;
import java.util.UUID;

public class Fixtures {
    public static UserId userId() {
        Random random = new Random();
        long id = Math.abs(random.nextLong());
        return UserId.fromLong(id).get();
    }

    public static UserName firstName() {
        Either<UserName.CreateUserNameError, UserName> result;
        do {
            Faker faker = new Faker();
            String value = faker.name().firstName();
            result = UserName.fromString(value);
        } while (result.isLeft());
        return result.get();
    }

    public static UserName lastName() {
        Either<UserName.CreateUserNameError, UserName> result;
        do {
            Faker faker = new Faker();
            String value = faker.name().lastName();
            result = UserName.fromString(value);
        } while (result.isLeft());
        return result.get();
    }

    public static UserLogin login() {
        return UserLogin.fromString(firstName().toStringValue().toLowerCase() + "." + lastName().toStringValue().toLowerCase()).get();
    }

    public static UserLogin login(String firstName, String lastName) {

        String value = firstName.toLowerCase() + "." + lastName.toLowerCase();
        return UserLogin.fromString(value).get();
    }

    public static UserName patronymicName() {
        Faker faker = new Faker();
        return UserName.fromString(faker.name().firstName()).get();
    }

    public static UserEntity user() {
        var firstName = firstName();
        var lastName = lastName();
        UserLogin userLogin = login(firstName.toStringValue(), lastName.toStringValue());

        return UserRestorer.restoreUser(
                userId(),
                userLogin,
                firstName,
                patronymicName(),
                lastName,
                false,
                new Version(),
                Optional.empty()
        );
    }

    public static UserEntity userWithPhoto(PhotoId photoId) {
        var firstName = firstName();
        var lastName = lastName();
        UserLogin userLogin = login(firstName.toStringValue(), lastName.toStringValue());

        return UserRestorer.restoreUser(
                userId(),
                userLogin,
                firstName,
                patronymicName(),
                lastName,
                false,
                new Version(),
                Optional.of(photoId)
        );
    }

    public static UserEntity user(UserId userId) {
        var firstName = firstName();
        var lastName = lastName();
        UserLogin userLogin = login(firstName.toStringValue(), lastName.toStringValue());

        return UserRestorer.restoreUser(
                userId,
                userLogin,
                firstName,
                patronymicName(),
                lastName,
                false,
                new Version(),
                Optional.empty()
        );
    }

    public static UserEntity removedUser() {
        var firstName = firstName();
        var lastName = lastName();
        UserLogin userLogin = login(firstName.toStringValue(), lastName.toStringValue());

        return UserRestorer.restoreUser(
                userId(),
                userLogin,
                firstName,
                patronymicName(),
                lastName,
                true,
                new Version(),
                Optional.empty()
        );
    }

    public static PhotoId photoId() {
        String uuid = UUID.randomUUID().toString();
        return PhotoId.fromString(uuid).get();
    }
}

