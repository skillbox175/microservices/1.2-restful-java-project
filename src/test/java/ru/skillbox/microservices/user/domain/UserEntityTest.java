package ru.skillbox.microservices.user.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.events.UserAddedDomainEvent;
import ru.skillbox.microservices.user.domain.events.UserRemovedDomainEvent;
import ru.skillbox.microservices.user.domain.events.UserUpdatedDomainEvent;

import static org.junit.jupiter.api.Assertions.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;

class UserEntityTest {
    final UserId userId = userId();
    final UserIdGenerator idGenerator = () -> UserEntityTest.this.userId;

    @Test
    void addUser_Success() {
        UserName firstName = firstName();
        UserName patronymicName = patronymicName();
        UserName lastName = lastName();
        UserLogin login = login(firstName.toStringValue(), lastName.toStringValue());

        var userAlreadyExists = new MockUserAlreadyExists();

        var result = UserEntity.addUser(
                idGenerator,
                userAlreadyExists,
                login,
                firstName,
                patronymicName,
                lastName
        );

        assertTrue(result.isRight());
        var user = result.get();

        assertEquals(user.id, userId);
        assertEquals(user.userLogin, login);
        assertEquals(user.firstName, firstName);
        assertEquals(user.patronymicName, patronymicName);
        assertEquals(user.lastName, lastName);
        assertFalse(user.isDeleted());
        var events = user.popEvents();

        assertEquals(1, events.size());
        assertEquals(events.get(0).getClass(), UserAddedDomainEvent.class);
        assertEquals(((UserAddedDomainEvent) events.get(0)).getUserId(), userId);
        assertEquals(((UserAddedDomainEvent) events.get(0)).getPayload(), user);
    }

    @Test
    void addUser_AlreadyExistsWithTheSameLogin() {
        UserName firstName = firstName();
        UserName patronymicName = patronymicName();
        UserName lastName = lastName();
        UserLogin login = login(firstName.toStringValue(), lastName.toStringValue());

        var userAlreadyExists = new MockUserAlreadyExists(true);

        var result = UserEntity.addUser(
                idGenerator,
                userAlreadyExists,
                login,
                firstName,
                patronymicName,
                lastName
        );

        assertEquals(result.getLeft().getClass(), UserEntity.AlreadyExistsWithSameLoginError.class);
    }

    @Test
    void removeUser_Success() {
        UserEntity user = user();
        user.removeUser();

        assertTrue(user.deleted);
        assertFalse(user.visible());

        var eventList = user.popEvents();
        assertEquals(1, eventList.size());
        assertEquals(eventList.get(0).getClass(), UserRemovedDomainEvent.class);
        assertEquals(user.id, ((UserRemovedDomainEvent) eventList.get(0)).getUserId());

    }

    @Test
    void removeUser_AlreadyRemoved() {
        UserEntity user = removedUser();
        user.removeUser();

        assertTrue(user.deleted);
        assertFalse(user.visible());
        assertTrue(user.popEvents().isEmpty());
    }

    @Test
    void updateUser_Successful() {
        UserEntity user = user();
        var newPatronimycName = patronymicName();

        var result = user.updateUser(
                user.userLogin,
                user.firstName,
                newPatronimycName,
                user.lastName
        );
        assertTrue(result.isRight());
        var updatedUser = result.get();

        assertEquals(user.id, updatedUser.id);
        assertEquals(user.userLogin, updatedUser.userLogin);
        assertEquals(user.firstName, updatedUser.firstName);
        assertEquals(newPatronimycName, updatedUser.patronymicName);
        assertEquals(user.lastName, updatedUser.lastName);
        assertFalse(updatedUser.isDeleted());

        var eventList = updatedUser.popEvents();
        assertEquals(1, eventList.size());
        assertEquals(UserUpdatedDomainEvent.class, eventList.get(0).getClass());
        assertEquals(user.id, ((UserUpdatedDomainEvent) eventList.get(0)).getUserId());
    }


    @Test
    void addPhoto() {
        UserEntity user = user();
        var photoId = photoId();

        var updatedUser = user.setPhoto(photoId).get();

        assertTrue(updatedUser.photoId.isPresent());
        assertEquals(photoId, updatedUser.photoId.get());

        var eventList = updatedUser.popEvents();
        assertEquals(1, eventList.size());
        assertEquals(UserUpdatedDomainEvent.class, eventList.get(0).getClass());
        assertEquals(user.id, ((UserUpdatedDomainEvent) eventList.get(0)).getUserId());
    }

    @Test
    void removePhoto() {
        UserEntity user = user();
        var photoId = photoId();

        var userWithPhoto = user.setPhoto(photoId).get();

        var userWithoutPhoto = userWithPhoto.removePhoto().get();

        assertTrue(userWithoutPhoto.photoId.isEmpty());

        var eventList = userWithoutPhoto.popEvents();
        assertEquals(1, eventList.size());
        assertEquals(UserUpdatedDomainEvent.class, eventList.get(0).getClass());
        assertEquals(user.id, ((UserUpdatedDomainEvent) eventList.get(0)).getUserId());
    }

    @AllArgsConstructor
    @NoArgsConstructor
    static class MockUserAlreadyExists implements UserAlreadyExists {
        boolean isExist = false;

        @Override
        public boolean invoke(UserLogin userLogin) {
            return this.isExist;
        }

        @Override
        public boolean invoke(UserId userId) {
            return this.isExist;
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor
    static class MockUserAlreadyFollowing implements UserAlreadyFollowing {
        boolean isFollowing = false;

        @Override
        public boolean invoke(UserId followingId, UserId currUserId) {
            return this.isFollowing;
        }
    }

}
