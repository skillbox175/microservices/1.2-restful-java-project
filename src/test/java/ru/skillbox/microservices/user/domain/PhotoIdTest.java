package ru.skillbox.microservices.user.domain;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PhotoIdTest {
    @Test
    void testFromString_EmptyValue() {
        String emptyValue = "";
        Either<PhotoId.CreatePhotoIdError, PhotoId> result = PhotoId.fromString(emptyValue);
        assertTrue(result.isLeft());
        assertInstanceOf(PhotoId.EmptyStringValue.class, result.getLeft());
    }

    @Test
    void testFromString_WhitespaceValue() {
        String whitespaceValue = "   ";
        Either<PhotoId.CreatePhotoIdError, PhotoId> result = PhotoId.fromString(whitespaceValue);
        assertTrue(result.isLeft());
        assertInstanceOf(PhotoId.EmptyStringValue.class, result.getLeft());
    }

    @Test
    void testFromString_ValidValue() {
        String validValue = "123e4567-e89b-12d3-a456-426614174000";
        Either<PhotoId.CreatePhotoIdError, PhotoId> result = PhotoId.fromString(validValue);
        assertTrue(result.isRight());
        assertEquals(validValue, result.get().toStringValue());
    }

    @Test
    void testFromString_InvalidValue() {
        String invalidValue = "123e4567-e89b-12d3-a456-4266141740-asdf";
        Either<PhotoId.CreatePhotoIdError, PhotoId> result = PhotoId.fromString(invalidValue);
        assertTrue(result.isLeft());
        assertInstanceOf(PhotoId.WrongStringValue.class, result.getLeft());
    }
}