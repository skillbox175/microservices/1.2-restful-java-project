package ru.skillbox.microservices.user.persistence.postgres;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import ru.skillbox.microservices.user.domain.PhotoId;
import ru.skillbox.microservices.user.domain.UserId;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;
import static ru.skillbox.microservices.user.persistence.postgres.Fixtures.*;

@SpringJUnitConfig(classes = {TestConfiguration.class})
class PostgresRepositoryWriteTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Test
    void save_new_instance_successful() {
        //given
        var publisher = new MockEventPublisher();
        var postgresUserRepository = new PostgresUserRepository(dataSource, publisher);
        var user = newUserWithPhoto();

        //when
        postgresUserRepository.save(user);

        //then
        checkExistsInDatabase(user, jdbcTemplate);
    }

    @Test
    void save_new_instance_but_already_exists_with_the_same_id() {
        //given
        var publisher = new MockEventPublisher();
        var postgresUserRepository = new PostgresUserRepository(dataSource, publisher);
        UserId userId = userId();
        var first = newUser(userId, firstName(), patronymicName(), lastName());
        var second = newUser(userId, firstName(), patronymicName(), lastName());

        //when
        postgresUserRepository.save(first);

        //then
        assertThrowsExactly(StorageConflictException.class, () -> postgresUserRepository.save(second));
        checkExistsInDatabase(first, jdbcTemplate);
        assertEquals(1, publisher.getEventListSize());
    }

    @Test
    void save_new_instance_and_then_update_it() {
        //given
        var publisher = new MockEventPublisher();
        var postgresUserRepository = new PostgresUserRepository(dataSource, publisher);
        var user = newUser();

        //when
        postgresUserRepository.save(user);
        user.removeUser();
        postgresUserRepository.save(user);

        //then
        checkExistsInDatabase(user, jdbcTemplate);
        assertEquals(2, publisher.getEventListSize());
    }

    @Test
    void follow_user_successful() {
        //given
        var publisher = new MockEventPublisher();
        var postgresUserRepository = new PostgresUserRepository(dataSource, publisher);
        var followerId = userId();
        var followingId = userId();

        var follower = user(followerId);
        var followerWithEvents = newUser(follower.id, follower.firstName, follower.lastName, follower.patronymicName);
        var following = user(followingId);
        var followingWithEvents = newUser(following.id, following.firstName, following.lastName, following.patronymicName);

        postgresUserRepository.save(followerWithEvents);
        postgresUserRepository.save(followingWithEvents);

        //when
        postgresUserRepository.follow(followingId, followerId);

        //then
        var result = postgresUserRepository.getFollowingById(followerId);
        assertEquals(result.stream().findFirst().get().id, followingId);
    }

    @Test
    void unfollow_user_successful() {
        //given
        var publisher = new MockEventPublisher();
        var postgresUserRepository = new PostgresUserRepository(dataSource, publisher);
        var followerId = userId();
        var followingId = userId();

        var follower = user(followerId);
        var followerWithEvents = newUser(follower.id, follower.firstName, follower.lastName, follower.patronymicName);
        var following = user(followingId);
        var followingWithEvents = newUser(following.id, following.firstName, following.lastName, following.patronymicName);

        postgresUserRepository.save(followerWithEvents);
        postgresUserRepository.save(followingWithEvents);
        postgresUserRepository.follow(followingId, followerId);

        //when
        postgresUserRepository.unfollow(followingId, followerId);

        //then
        var result = postgresUserRepository.getFollowingById(followerId);
        assertTrue(result.isEmpty());
    }

    @Test
    void setPhoto_for_user_successful() {
        //given
        var publisher = new MockEventPublisher();
        var postgresUserRepository = new PostgresUserRepository(dataSource, publisher);
        var user = newUser();
        PhotoId photoId = photoId();

        //when
        user.setPhoto(photoId);
        postgresUserRepository.save(user);

        //then
        checkExistsInDatabase(user, jdbcTemplate);
        assertEquals(2, publisher.getEventListSize());
    }
}
