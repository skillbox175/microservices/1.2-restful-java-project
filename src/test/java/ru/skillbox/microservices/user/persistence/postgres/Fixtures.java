package ru.skillbox.microservices.user.persistence.postgres;

import lombok.NoArgsConstructor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.skillbox.microservices.user.common.events.DomainEventPublisher;
import ru.skillbox.microservices.user.common.types.base.DomainEvent;
import ru.skillbox.microservices.user.domain.*;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserAlreadyExists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;

public class Fixtures {
    public static UserEntity newUser(final UserId userId, UserName firstName, UserName lastName, UserName patronymicName) {
        var generator = new UserIdGenerator() {
            @Override
            public UserId generate() {
                return userId;
            }
        };

        var result = UserEntity.addUser(
                generator,
                new MockUserAlreadyExists(false),
                login(firstName.toStringValue(), lastName.toStringValue()),
                firstName,
                lastName,
                patronymicName
        );

        assertTrue(result.isRight());
        return result.get();
    }

    public static UserEntity newUser() {
        UserId userId = userId();
        UserName firstName = firstName();
        UserName lastName = lastName();
        UserName patronymicName = patronymicName();

        var generator = new UserIdGenerator() {
            @Override
            public UserId generate() {
                return userId;
            }
        };

        var result = UserEntity.addUser(
                generator,
                new MockUserAlreadyExists(false),
                login(firstName.toStringValue(), lastName.toStringValue()),
                firstName,
                lastName,
                patronymicName
        );

        assertTrue(result.isRight());
        return result.get();
    }

    public static UserEntity newUserWithPhoto() {
        UserEntity user = newUser();
        PhotoId photoId = photoId();

        var result = user.setPhoto(photoId);

        assertTrue(result.isRight());
        return result.get();
    }

    public static void checkExistsInDatabase(UserEntity user, NamedParameterJdbcTemplate jdbcTemplate) {
        Map<String, ?> params = Map.of(
                "id", user.id.toLongValue(),
                "login", user.userLogin.toStringValue(),
                "firstName", user.firstName.toStringValue(),
                "patronymicName", user.patronymicName.toStringValue(),
                "lastName", user.lastName.toStringValue(),
                "gender", 1L,
                "isDeleted", user.isDeleted(),
                "version", user.version.toLongValue(),
                "photoId", user.photoId.map(PhotoId::toStringValue).orElse("NULL")
        );
        boolean exists = Boolean.TRUE.equals(jdbcTemplate.queryForObject(
                "SELECT EXISTS(SELECT 1 FROM public.users WHERE " +
                        "id = :id AND " +
                        "login = :login AND " +
                        "first_name = :firstName AND " +
                        "patronymic_name = :patronymicName AND " +
                        "last_name = :lastName AND " +
                        "deleted = :isDeleted AND " +
                        "version = :version AND " +
                        "photo_id = :photoId)", params, Boolean.class));
        assertTrue(exists);
    }

    public static void cleanUsersTable(NamedParameterJdbcTemplate jdbcTemplate) {
        jdbcTemplate.getJdbcTemplate().execute("TRUNCATE TABLE public.users");
    }

    public static void checkEquals(UserEntity user1, UserEntity user2) {
        assertEquals(user1.id, user2.id);
        assertEquals(user1.userLogin, user2.userLogin);
        assertEquals(user1.firstName, user2.firstName);
        assertEquals(user1.patronymicName, user2.patronymicName);
        assertEquals(user1.lastName, user2.lastName);
        assertEquals(user1.deleted, user2.deleted);
    }

    @NoArgsConstructor
    static class MockEventPublisher implements DomainEventPublisher {
        private final List<DomainEvent> events = new ArrayList<>();

        @Override
        public void publish(Collection<? extends DomainEvent> events) {
            this.events.addAll(events);
        }

        public void verifyConstrains(Collection<? extends DomainEvent> events) {
            assertThat(this.events).containsExactlyInAnyOrder((DomainEvent) events);
        }

        public void verifyEventListIsEmpty(Collection<? extends DomainEvent> events) {
            assertTrue(events.isEmpty());
        }

        public int getEventListSize() {
           return this.events.size();
        }
    }
}
