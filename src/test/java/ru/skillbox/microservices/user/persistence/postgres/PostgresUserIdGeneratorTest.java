package ru.skillbox.microservices.user.persistence.postgres;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import ru.skillbox.microservices.user.domain.testFixtures.Fixtures;
import ru.skillbox.microservices.user.domain.UserId;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringJUnitConfig(classes = {TestConfiguration.class})
class PostgresUserIdGeneratorTest {
    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private DataSource dataSource;

    @Test
    void generate_id() {
        UserId id = Fixtures.userId();
        jdbcTemplate.getJdbcTemplate().execute("SELECT setval('public.users_id_seq', " + id.toLongValue() + ");");
        var generator = new PostgresUserIdGenerator(dataSource);

        var userId = generator.generate();

        assertEquals(userId, UserId.fromLong(id.toLongValue() + 1).get());
    }
}
