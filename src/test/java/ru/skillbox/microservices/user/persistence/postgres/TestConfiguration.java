package ru.skillbox.microservices.user.persistence.postgres;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.testcontainers.containers.PostgreSQLContainer;
import ru.skillbox.microservices.user.common.events.DomainEventPublisher;
import ru.skillbox.microservices.user.persistence.postgres.Fixtures.MockEventPublisher;

import javax.sql.DataSource;

@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {
    @Bean
    public SpringLiquibase liquibase(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:/db/changelog/db.changelog-master.yaml");
        liquibase.setDataSource(dataSource);
        return liquibase;
    }

    @Bean
    public SingleConnectionDataSource dataSource(PostgreSQLContainer container) {
        return new SingleConnectionDataSource(
                container.getJdbcUrl(),
                container.getUsername(),
                container.getPassword(),
                true);
    }

    @Bean(initMethod = "start")
    public PostgreSQLContainer postgresqlContainer() {
        return new PostgreSQLContainer("postgres:15.1-alpine");
    }

    @Bean
    public NamedParameterJdbcTemplate jdbcTemplateBean(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public DomainEventPublisher eventPublisher(){
        return new MockEventPublisher();
    }
}
