package ru.skillbox.microservices.user.persistence.postgres;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import ru.skillbox.microservices.user.persistence.postgres.Fixtures.MockEventPublisher;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;
import static ru.skillbox.microservices.user.persistence.postgres.Fixtures.checkEquals;
import static ru.skillbox.microservices.user.persistence.postgres.Fixtures.newUserWithPhoto;

@SpringJUnitConfig(classes = {TestConfiguration.class})
class PostgresRepositoryReadTest {
    @Autowired
    private DataSource dataSource;

    @Test
    void getById_notFound() {
        var publisher = new MockEventPublisher();
        var postgresUserRepository = new PostgresUserRepository(dataSource, publisher);
        var result = postgresUserRepository.getById(userId());

        assertTrue(result.isEmpty());
    }

    @Test
    void getById_successfullyReturned() {
        var user = newUserWithPhoto();
        var publisher = new MockEventPublisher();
        var repository = new PostgresUserRepository(dataSource, publisher);
        repository.save(user);

        var result = repository.getById(user.id);
        assertTrue(result.isPresent());
        checkEquals(result.get(), user);
    }
}
