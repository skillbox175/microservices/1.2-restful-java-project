package ru.skillbox.microservices.user.usecase.scenarios;

import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.FollowUser;
import ru.skillbox.microservices.user.usecase.scenario.FollowUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserAlreadyExists;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserAlreadyFollowing;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;

class FollowUserUseCaseTest {
    @Test
    void successfully_start_following() {
        UserId followerId = userId();
        UserId followingId = userId();

        MockUserPersister userPersister = new MockUserPersister(user(followerId), new HashMap<>());
        MockUserAlreadyFollowing alreadyFollowing = new MockUserAlreadyFollowing(false);
        MockUserAlreadyExists alreadyExists = new MockUserAlreadyExists(true);

        FollowUserUseCase useCase = new FollowUserUseCase(userPersister, alreadyFollowing, alreadyExists);
        var result = useCase.execute(followerId, followingId);

        assertTrue(result.isRight());
        userPersister.verifyFollowing(followingId);
    }

    @Test
    void subscribe_already_following() {
        UserId followerId = userId();
        UserId followingId = userId();

        MockUserPersister userPersister = new MockUserPersister();
        MockUserAlreadyFollowing alreadyFollowing = new MockUserAlreadyFollowing(true);
        MockUserAlreadyExists alreadyExists = new MockUserAlreadyExists(true);

        FollowUserUseCase useCase = new FollowUserUseCase(userPersister, alreadyFollowing, alreadyExists);
        var result = useCase.execute(followerId, followingId);

        assertTrue(result.isLeft());
        assertEquals(result.getLeft().getClass(), FollowUser.UserAlreadySubscribed.class);
    }

    @Test
    void one_followed_not_found() {
        UserId followerId = userId();
        UserId followingId = userId();

        MockUserPersister userPersister = new MockUserPersister();
        MockUserAlreadyFollowing alreadyFollowing = new MockUserAlreadyFollowing(false);
        MockUserAlreadyExists alreadyExists = new MockUserAlreadyExists(false);

        FollowUserUseCase useCase = new FollowUserUseCase(userPersister, alreadyFollowing, alreadyExists);
        var result = useCase.execute(followerId, followingId);

        assertTrue(result.isLeft());
        assertEquals(result.getLeft().getClass(), FollowUser.FollowedNotFound.class);
    }
}
