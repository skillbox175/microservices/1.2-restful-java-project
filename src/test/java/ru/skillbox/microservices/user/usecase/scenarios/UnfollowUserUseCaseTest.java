package ru.skillbox.microservices.user.usecase.scenarios;

import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.usecase.UnfollowUser;
import ru.skillbox.microservices.user.usecase.scenario.UnfollowUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserAlreadyExists;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserAlreadyFollowing;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;

class UnfollowUserUseCaseTest {
    @Test
    void successfully_stop_following() {
        UserId followerId = userId();
        UserId followingId = userId();

        MockUserPersister userPersister = new MockUserPersister(user(followerId), new HashMap<>());
        userPersister.subscription.put(followingId, followerId);
        MockUserAlreadyFollowing alreadyFollowing = new MockUserAlreadyFollowing(true);
        MockUserAlreadyExists alreadyExists = new MockUserAlreadyExists(true);

        UnfollowUserUseCase useCase = new UnfollowUserUseCase(userPersister, alreadyFollowing, alreadyExists);
        var result = useCase.execute(followerId, followingId);

        assertTrue(result.isRight());
        userPersister.verifyFollowingIsEmpty();
    }

    @Test
    void cannot_unfollowing_not_following() {
        UserId followerId = userId();
        UserId followingId = userId();

        MockUserPersister userPersister = new MockUserPersister(user(followerId), new HashMap<>());
        MockUserAlreadyFollowing alreadyFollowing = new MockUserAlreadyFollowing(false);
        MockUserAlreadyExists alreadyExists = new MockUserAlreadyExists(true);

        UnfollowUserUseCase useCase = new UnfollowUserUseCase(userPersister, alreadyFollowing, alreadyExists);
        var result = useCase.execute(followerId, followingId);

        assertTrue(result.isLeft());
        assertTrue(result.getLeft() instanceof UnfollowUser.NotFollowing);
        userPersister.verifyFollowingIsEmpty();
    }

    @Test
    void cannot_unfollowing_following_not_found() {
        UserId followerId = userId();
        UserId followingId = userId();

        MockUserPersister userPersister = new MockUserPersister(user(followerId), new HashMap<>());
        MockUserAlreadyFollowing alreadyFollowing = new MockUserAlreadyFollowing(false);
        MockUserAlreadyExists alreadyExists = new MockUserAlreadyExists(false);

        UnfollowUserUseCase useCase = new UnfollowUserUseCase(userPersister, alreadyFollowing, alreadyExists);
        var result = useCase.execute(followerId, followingId);

        assertTrue(result.isLeft());
        assertTrue(result.getLeft() instanceof UnfollowUser.UserNotFound);
        userPersister.verifyFollowingIsEmpty();
    }
}
