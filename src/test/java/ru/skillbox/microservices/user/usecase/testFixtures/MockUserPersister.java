package ru.skillbox.microservices.user.usecase.testFixtures;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;
import ru.skillbox.microservices.user.usecase.access.UserPersister;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

@AllArgsConstructor
@NoArgsConstructor
public final class MockUserPersister implements UserPersister {
    public UserEntity user;
    public HashMap<UserId, UserId> subscription = new HashMap<>();


    @Override
    public void save(UserEntity user) {
        this.user = user;
    }

    @Override
    public void follow(UserId followingId, UserId currUserId) {
        this.subscription.put(followingId, currUserId);
    }

    @Override
    public void unfollow(UserId followingId, UserId currUserId) {
        this.subscription.remove(followingId, currUserId);
    }

    public void verifyInvoked(UserEntity user) {
        assertEquals(this.user, user);
    }

    public void verifyEmpty() {
        assertNull(this.user);
    }

    public void verifyInvoked(UserLogin login, UserName firstName, UserName patronymicName, UserName lastName) {
        assertEquals(this.user.userLogin, login);
        assertEquals(this.user.firstName, firstName);
        assertEquals(this.user.patronymicName, patronymicName);
        assertEquals(this.user.lastName, lastName);
    }

    public void verifyFollowing(UserId followingId) {
        assertTrue(subscription.containsKey(followingId));
    }

    public void verifyFollowingIsEmpty() {
        assertTrue(subscription.isEmpty());
    }
}

