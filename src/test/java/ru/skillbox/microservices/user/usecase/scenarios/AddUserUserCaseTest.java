package ru.skillbox.microservices.user.usecase.scenarios;

import io.vavr.control.Either;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.*;
import ru.skillbox.microservices.user.domain.events.UserAddedDomainEvent;
import ru.skillbox.microservices.user.domain.testFixtures.Fixtures;
import ru.skillbox.microservices.user.usecase.AddUser;
import ru.skillbox.microservices.user.usecase.scenario.AddUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserAlreadyExists;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import static org.junit.jupiter.api.Assertions.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;

class AddUserUserCaseTest {
    @Test
    void successfully_added() {
        UserName firstName = firstName();
        UserName lastName = lastName();
        UserName patronymicName = patronymicName();
        UserLogin login = login(firstName.toStringValue(), lastName.toStringValue());

        MockUserPersister persister = new MockUserPersister();
        TestUserIdGenerator idGenerator = new TestUserIdGenerator();
        MockUserAlreadyExists userAlreadyExist = new MockUserAlreadyExists(false);

        AddUserUseCase useCase = new AddUserUseCase(
                persister,
                idGenerator,
                userAlreadyExist);
        Either<AddUser.AddUserUseCaseError, UserId> result = useCase.execute(
                login,
                firstName,
                patronymicName,
                lastName
        );

        UserId id = idGenerator.id;

        UserEntity user = persister.user;

        assertEquals(result.get(), id);
        persister.verifyInvoked(login, firstName, patronymicName, lastName);
        assertTrue(user.visible());

        var events = user.popEvents();
        assertEquals(1, events.size());
        events.forEach(event ->
                assertInstanceOf(UserAddedDomainEvent.class, event)
        );
    }

    @Test
    void user_already_exists() {
        UserName firstName = firstName();
        UserName lastName = lastName();
        UserName patronymicName = patronymicName();
        UserLogin login = login(firstName.toStringValue(), lastName.toStringValue());

        MockUserPersister persister = new MockUserPersister();
        TestUserIdGenerator idGenerator = new TestUserIdGenerator();
        MockUserAlreadyExists userAlreadyExist = new MockUserAlreadyExists(true);

        AddUserUseCase useCase = new AddUserUseCase(
                persister,
                idGenerator,
                userAlreadyExist);
        Either<AddUser.AddUserUseCaseError, UserId> result = useCase.execute(
                login,
                firstName,
                patronymicName,
                lastName
        );
        assertTrue(result.isLeft());
        assertEquals(result.getLeft().getClass(), AddUser.AlreadyExists.class);
    }

    @NoArgsConstructor
    static class TestUserIdGenerator implements UserIdGenerator {
        public UserId id;

        {
            id = Fixtures.userId();
        }


        @Override
        public UserId generate() {
            return id;
        }
    }
}

