package ru.skillbox.microservices.user.usecase.invariants;

import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.usecase.invariant.UserAlreadyFollowingUsingUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;

class UserAlreadyFollowingUsingUserExtractorTest {
    @Test
    void user_already_following() {
        var follower = user();
        var followingId = userId();
        var extractor = new MockUserExtractor(follower);
        extractor.followings.add(user(followingId));
        var rule = new UserAlreadyFollowingUsingUserExtractor(extractor);

        var result = rule.invoke(followingId, follower.id);
        assertTrue(result);

        extractor.verifyInvokedGetByFollowerId(follower.id);
    }

    @Test
    void user_not_following() {
        var follower = user();
        var followingId = userId();
        var extractor = new MockUserExtractor(follower);
        var rule = new UserAlreadyFollowingUsingUserExtractor(extractor);

        var result = rule.invoke(followingId, follower.id);
        assertFalse(result);

        extractor.verifyInvokedGetByFollowerId(follower.id);
    }
}
