package ru.skillbox.microservices.user.usecase.scenarios;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.PhotoId;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;
import ru.skillbox.microservices.user.usecase.scenario.RemovePhotoForUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.photoId;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userWithPhoto;

class RemovePhotoForUserUseCaseTest {
    @Test
    void removePhoto_Success() {
        // Arrange
        PhotoId photoId = photoId();
        UserEntity user = userWithPhoto(photoId);

        MockUserPersister persister = new MockUserPersister();
        persister.save(user);
        UserExtractor extractor = new MockUserExtractor(user);

        RemovePhotoForUserUseCase useCase = new RemovePhotoForUserUseCase(extractor, persister);

        // Act
        Either<RemovePhotoForUserUseCase.RemovePhotoForUserUseCaseError, UserInfo> result = useCase.execute(user.id);

        // Assert
        assertTrue(result.isRight());
        assertEquals(result.get().id(), user.id);
        assertEquals(result.get().photoId(), Optional.empty());
    }

    @Test
    void removePhoto_UserNotFound() {
        // Arrange
        UserEntity user = userWithPhoto(photoId());
        MockUserPersister persister = new MockUserPersister();
        UserExtractor extractor = new MockUserExtractor();

        RemovePhotoForUserUseCase useCase = new RemovePhotoForUserUseCase(extractor, persister);

        // Act
        Either<RemovePhotoForUserUseCase.RemovePhotoForUserUseCaseError, UserInfo> result = useCase.execute(user.id);

        // Assert
        assertTrue(result.isLeft());
        assertInstanceOf(RemovePhotoForUserUseCase.UserNotFound.class, result.getLeft());
    }
}
