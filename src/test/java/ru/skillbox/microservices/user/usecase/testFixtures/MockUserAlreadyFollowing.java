package ru.skillbox.microservices.user.usecase.testFixtures;

import lombok.RequiredArgsConstructor;
import ru.skillbox.microservices.user.domain.UserAlreadyFollowing;
import ru.skillbox.microservices.user.domain.UserId;

@RequiredArgsConstructor
public class MockUserAlreadyFollowing implements UserAlreadyFollowing {
    final boolean isFollowing;

    @Override
    public boolean invoke(UserId followingId, UserId currUserId) {
        return this.isFollowing;
    }
}
