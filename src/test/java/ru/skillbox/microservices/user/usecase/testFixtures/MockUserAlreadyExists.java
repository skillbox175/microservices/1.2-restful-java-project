package ru.skillbox.microservices.user.usecase.testFixtures;

import lombok.AllArgsConstructor;
import ru.skillbox.microservices.user.domain.UserAlreadyExists;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;

@AllArgsConstructor
public final class MockUserAlreadyExists implements UserAlreadyExists {
    boolean isExist;

    @Override
    public boolean invoke(UserLogin userLogin) {
        return isExist;
    }

    @Override
    public boolean invoke(UserId userId) {
        return isExist;
    }
}
