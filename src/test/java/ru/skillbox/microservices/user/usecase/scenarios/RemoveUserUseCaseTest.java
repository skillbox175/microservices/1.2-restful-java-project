package ru.skillbox.microservices.user.usecase.scenarios;

import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.usecase.RemoveUser;
import ru.skillbox.microservices.user.usecase.scenario.RemoveUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import static org.junit.jupiter.api.Assertions.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.userId;

class RemoveUserUseCaseTest {
    @Test
    void successfully_removed() {
        var user = user();
        var userPersister = new MockUserPersister();
        var userExtractor = new MockUserExtractor(user);

        var useCase = new RemoveUserUseCase(userExtractor, userPersister);
        var result = useCase.execute(user.id);

        assertTrue(result.isRight());
        assertNull(result.get());
        userPersister.verifyInvoked(user);
        userExtractor.verifyInvokedGetById(user.id);
        userExtractor.verifyInvokedGetById(user.id);
    }

    @Test
    void user_not_found() {

        var userPersister = new MockUserPersister();
        var userExtractor = new MockUserExtractor();
        var useCase = new RemoveUserUseCase(userExtractor, userPersister);

        var userId = userId();
        var result = useCase.execute(userId);

        assertTrue(result.isLeft());
        assertEquals(result.getLeft().getClass(), RemoveUser.UserNotFound.class);

        userPersister.verifyEmpty();
        userExtractor.verifyInvokedGetById(userId);
    }

}
