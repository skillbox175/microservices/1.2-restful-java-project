package ru.skillbox.microservices.user.usecase.scenarios;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserName;
import ru.skillbox.microservices.user.domain.UserRestorer;
import ru.skillbox.microservices.user.usecase.UpdateUser;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;
import ru.skillbox.microservices.user.usecase.scenario.UpdateUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.patronymicName;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;
import static ru.skillbox.microservices.user.persistence.postgres.Fixtures.newUser;

class UpdateUserUseCaseTest {
    @Test
    void successfully_updated() {
        UserEntity user = newUser();

        MockUserPersister persister = new MockUserPersister();
        persister.save(user);
        UserExtractor extractor = new MockUserExtractor(user);
        UserName newPatronymicName = patronymicName();

        UserEntity updatedUser = UserRestorer.restoreUser(user.id,
                user.userLogin,
                user.firstName,
                newPatronymicName,
                user.lastName,
                user.deleted,
                user.version.next(),
                Optional.empty());

        UpdateUserUseCase useCase = new UpdateUserUseCase(
                persister,
                extractor);
        Either<UpdateUser.UpdateUserUseCaseError, UserInfo> result = useCase.execute(
                user.id,
                user.userLogin,
                user.firstName,
                newPatronymicName,
                user.lastName
        );

        assertEquals(result.get().id(), user.id);
        var resultUser = persister.user;
        assertEquals(resultUser.id, updatedUser.id);
        assertEquals(resultUser.userLogin, updatedUser.userLogin);
        assertEquals(resultUser.firstName, updatedUser.firstName);
        assertEquals(resultUser.patronymicName, updatedUser.patronymicName);
        assertEquals(resultUser.lastName, updatedUser.lastName);
        assertEquals(resultUser.deleted, updatedUser.deleted);
        assertEquals(resultUser.version, updatedUser.version);
        assertEquals(resultUser.photoId, updatedUser.photoId);
    }

    @Test
    void user_not_found() {
        UserEntity user = user();

        MockUserPersister persister = new MockUserPersister();
        UserExtractor userExtractor = new MockUserExtractor();

        UpdateUserUseCase useCase = new UpdateUserUseCase(
                persister,
                userExtractor);
        Either<UpdateUser.UpdateUserUseCaseError, UserInfo> result = useCase.execute(
                user.id,
                user.userLogin,
                user.firstName,
                user.patronymicName,
                user.lastName
        );
        assertTrue(result.isLeft());
        assertEquals(result.getLeft().getClass(), UpdateUser.UserNotFound.class);
    }
}
