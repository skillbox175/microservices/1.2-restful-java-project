package ru.skillbox.microservices.user.usecase.testFixtures;

import lombok.NoArgsConstructor;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@NoArgsConstructor
public final class MockUserExtractor implements UserExtractor {
    Boolean all = false;
    public UserEntity user;
    UserId id;
    UserLogin login;

    public Set<UserEntity> followings = new HashSet<>();
    public Set<UserEntity> followers = new HashSet<>();
    UserId followerId;
    UserId followingId;

    public MockUserExtractor(UserEntity user) {
        this.user = user;
    }

    public void verifyInvokedGetById(UserId userId) {
        assertEquals(userId, this.id);
        assertEquals(false, this.all);
    }

    public void verifyInvokedGetByLogin(UserLogin login) {
        assertEquals(login, this.login);
        assertEquals(false, this.all);
    }

    public void verifyInvokedGetByFollowingId(UserId followingId) {
        assertEquals(followingId, this.followingId);
        assertEquals(false, this.all);
    }

    public void verifyInvokedGetByFollowerId(UserId followerId) {
        assertEquals(followerId, this.followerId);
        assertEquals(false, this.all);
    }

    public void verifyInvokeGetAll() {
        assertTrue(this.all);
        assertNull(this.id);
        assertNull(this.login);
    }

    @Override
    public Optional<UserEntity> getById(UserId id) {
        this.id = id;
        if (this.user != null && Objects.equals(this.user.id, id)) {
            return Optional.of(this.user);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserEntity> getByLogin(UserLogin login) {
        this.login = login;
        if (this.user != null && this.user.userLogin == login) {
            return Optional.of(this.user);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<UserEntity> getAll() {
        this.all = true;
        if (this.user!= null) return List.of(this.user);
        else return new ArrayList<>();
    }

    @Override
    public Set<UserEntity> getFollowingById(UserId followerId) {
        this.followerId = followerId;
        if (this.user != null && this.user.id == followerId)
            return this.followings;
        else
            return new HashSet<>();
    }

    @Override
    public Set<UserEntity> getFollowerById(UserId followingId) {
        this.followingId = followingId;
        if (this.user != null && this.user.id == followingId)
            return this.followers;
        else
            return new HashSet<>();
    }
}
