package ru.skillbox.microservices.user.usecase.scenarios;

import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;
import ru.skillbox.microservices.user.usecase.scenario.GetAllUsersUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;

class GetAllUsersUseCaseTest {
    @Test
    void getAllUsers_empty_list() {
        var userExtractor = new MockUserExtractor();
        var useCase = new GetAllUsersUseCase(userExtractor);
        var allUsers = useCase.execute();

        assertTrue(allUsers.isEmpty());
        userExtractor.verifyInvokeGetAll();
    }

    @Test
    void get_all_users() {
        var user = user();
        var userExtractor = new MockUserExtractor(user);

        var useCase = new GetAllUsersUseCase(userExtractor);
        var allUsers = useCase.execute();

        assertEquals(Stream.of(UserInfo.from(user)).toList(), allUsers);
        userExtractor.verifyInvokeGetAll();
    }
}
