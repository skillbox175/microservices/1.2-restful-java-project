package ru.skillbox.microservices.user.usecase.invariants;

import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.usecase.invariant.UserAlreadyExistUsingUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;

class UserAlreadyExistsUsingUserExtractorTest {
    @Test
    void user_already_exists() {
        var user = user();
        var extractor = new MockUserExtractor(user);
        var rule = new UserAlreadyExistUsingUserExtractor(extractor);

        var result = rule.invoke(user.userLogin);
        assertTrue(result);

        extractor.verifyInvokedGetByLogin(user.userLogin);
    }

    @Test
    void user_already_exists_but_removed() {
        var user = removedUser();
        var extractor = new MockUserExtractor(user);
        var rule = new UserAlreadyExistUsingUserExtractor(extractor);

        var result = rule.invoke(user.userLogin);
        assertFalse(result);

        extractor.verifyInvokedGetByLogin(user.userLogin);
    }

    @Test
    void user_not_exits() {
        var extractor = new MockUserExtractor();
        var rule = new UserAlreadyExistUsingUserExtractor(extractor);

        var login = login();
        var result = rule.invoke(login);
        assertFalse(result);

        extractor.verifyInvokedGetByLogin(login);
    }
}
