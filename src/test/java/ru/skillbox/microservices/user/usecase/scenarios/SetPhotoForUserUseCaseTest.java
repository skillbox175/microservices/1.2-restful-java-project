package ru.skillbox.microservices.user.usecase.scenarios;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.PhotoId;
import ru.skillbox.microservices.user.domain.UserEntity;
import ru.skillbox.microservices.user.usecase.SetPhotoForUser;
import ru.skillbox.microservices.user.usecase.access.UserExtractor;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;
import ru.skillbox.microservices.user.usecase.scenario.SetPhotoForUserUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserPersister;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;

class SetPhotoForUserUseCaseTest {

    @Test
    void setPhoto_Success() {
        // Arrange
        UserEntity user = user();

        MockUserPersister persister = new MockUserPersister();
        persister.save(user);
        UserExtractor extractor = new MockUserExtractor(user);

        PhotoId photoId = photoId();

        SetPhotoForUserUseCase useCase = new SetPhotoForUserUseCase(extractor, persister);

        // Act
        Either<SetPhotoForUser.SetPhotoForUserUseCaseError, UserInfo> result = useCase.execute(user.id, photoId);

        // Assert
        assertTrue(result.isRight());
        assertEquals(result.get().id(), user.id);
        assertEquals(result.get().photoId(), Optional.of(photoId));
    }


    @Test
    void setPhoto_UserNotFound() {
        // Arrange
        UserEntity user = user();

        MockUserPersister persister = new MockUserPersister();
        UserExtractor extractor = new MockUserExtractor();

        PhotoId photoId = photoId();

        SetPhotoForUserUseCase useCase = new SetPhotoForUserUseCase(extractor, persister);

        // Act
        Either<SetPhotoForUser.SetPhotoForUserUseCaseError, UserInfo> result = useCase.execute(user.id, photoId);

        // Assert
        assertTrue(result.isLeft());
        assertInstanceOf(SetPhotoForUser.UserNotFound.class, result.getLeft());
    }

    @Test
    void testExecute_UserRemoved() {
        // Arrange
        UserEntity user = removedUser();

        MockUserPersister persister = new MockUserPersister();
        persister.save(user);
        UserExtractor extractor = new MockUserExtractor(user);

        PhotoId photoId = photoId();

        SetPhotoForUserUseCase useCase = new SetPhotoForUserUseCase(extractor, persister);

        // Act
        Either<SetPhotoForUser.SetPhotoForUserUseCaseError, UserInfo> result = useCase.execute(user.id, photoId);

        // Assert
        assertTrue(result.isLeft());
        assertInstanceOf(SetPhotoForUser.UserNotFound.class, result.getLeft());
    }
}
