package ru.skillbox.microservices.user.usecase.scenarios;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.testFixtures.Fixtures;
import ru.skillbox.microservices.user.usecase.GetUserById;
import ru.skillbox.microservices.user.usecase.dto.UserInfo;
import ru.skillbox.microservices.user.usecase.scenario.GetUserByIdUseCase;
import ru.skillbox.microservices.user.usecase.testFixtures.MockUserExtractor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.removedUser;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;

class GetUserByIdUseCaseTest {
    @Test
    void user_not_found() {
        MockUserExtractor userExtractor = new MockUserExtractor();
        GetUserByIdUseCase useCase = new GetUserByIdUseCase(userExtractor);

        UserId userId = Fixtures.userId();
        Either<GetUserById.GetUserByIdUseCaseError, UserInfo> result = useCase.execute(userId);

        assertTrue(result.isLeft());
        userExtractor.verifyInvokedGetById(userId);
    }

    @Test
    void user_removed() {
        var user = removedUser();
        var userExtractor = new MockUserExtractor(user);
        var useCase = new GetUserByIdUseCase(userExtractor);

        var result = useCase.execute(user.id);

        assertTrue(result.isLeft());
        assertEquals(GetUserById.UserNotFound.class
                , result.getLeft().getClass());
        userExtractor.verifyInvokedGetById(user.id);
    }

    @Test
    void user_extracted_successfully() {
        var user = user();
        var userExtractor = new MockUserExtractor(user);
        var useCase = new GetUserByIdUseCase(userExtractor);

        var result = useCase.execute(user.id);
        assertTrue(result.isRight());
        var userInfo = result.get();

        assertEquals(userInfo, UserInfo.from(user));
    }
}
