#!/bin/sh

curl -X 'POST' \
  'http://localhost:8008/users' \
  -H 'Content-Type: application/json' \
  -d '{
  "firstName": "Vanya",
  "lastName": "Ivanov"
}'

curl -X 'POST' \
  'http://localhost:8008/users' \
  -H 'Content-Type: application/json' \
  -d '{
  "firstName": "Petya",
  "lastName": "Ivanov"
}'

curl -X 'POST' \
  'http://localhost:8008/users' \
  -H 'Content-Type: application/json' \
  -d '{
  "firstName": "Zhenya",
  "lastName": "Ivanov"
}'