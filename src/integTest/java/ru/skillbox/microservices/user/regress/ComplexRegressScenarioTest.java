package ru.skillbox.microservices.user.regress;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.skillbox.microservices.user.application.configuration.new_gen.ApplicationNewGenConfiguration;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.persistence.postgres.PostgresUserIdGenerator;
import ru.skillbox.microservices.user.persistence.postgres.TestConfiguration;
import ru.skillbox.microservices.user.rest.v1.AddUserEndpoint;
import ru.skillbox.microservices.user.rest.v1.UpdateUserEndpoint;
import ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures;

import javax.sql.DataSource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.firstName;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.user;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.*;
import static ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures.notFoundTypeUrl;

@WebMvcTest
@ContextConfiguration(classes = {TestConfiguration.class, ApplicationNewGenConfiguration.class})
@Slf4j
class ComplexRegressScenarioTest {


    private final DataSource dataSource;


    private final MockMvc mockMvc;

    @Autowired
    ComplexRegressScenarioTest(DataSource dataSource, MockMvc mockMvc) {
        this.dataSource = dataSource;
        this.mockMvc = mockMvc;
    }
    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    @SneakyThrows
    void complexSubscriptionRegressScenario() {
        var generator = new PostgresUserIdGenerator(dataSource);

        var userIdPrevious = generator.generate();
        var userId1 = UserId.fromLong(userIdPrevious.toLongValue() + 1).get();
        var userId2 = UserId.fromLong(userId1.toLongValue() + 1).get();

        var user1 = user(userId1);
        var user2 = user(userId2);

        log.info("======================Create 2 users======================");
        var postUrl = new Fixtures.UrlBuilder(API_V1_USERS_ADD).withHost().build();
        mockMvc.perform(post(postUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        mapper.writeValueAsString(
                                new AddUserEndpoint.AddUserRestRequest(
                                        user1.userLogin.toStringValue(),
                                        user1.firstName.toStringValue(),
                                        user1.patronymicName.toStringValue(),
                                        user1.lastName.toStringValue()
                                )
                        )
                )
        ).andExpectAll(
                content().string(""),
                status().isCreated()
        );
        mockMvc.perform(post(postUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        mapper.writeValueAsString(
                                new AddUserEndpoint.AddUserRestRequest(
                                        user2.userLogin.toStringValue(),
                                        user2.firstName.toStringValue(),
                                        user2.patronymicName.toStringValue(),
                                        user2.lastName.toStringValue()
                                )
                        )
                )
        ).andExpectAll(
                content().string(""),
                status().isCreated()
        );

        log.info("======================Find users by Id======================");
        String postUrl1 = new Fixtures.UrlBuilder(API_V1_USERS_GET_BY_ID)
                .withId(userId1.toLongValue()).withHost().build();
        String postUrl2 = new Fixtures.UrlBuilder(API_V1_USERS_GET_BY_ID)
                .withId(userId2.toLongValue()).withHost().build();

        mockMvc.perform(get(postUrl1)).andExpect(status().isOk());
        mockMvc.perform(get(postUrl2)).andExpect(status().isOk());

        log.info("======================Subscribe user2 to user1======================");
        String followUrl = new Fixtures.UrlBuilder(API_V1_USERS_FOLLOW_BY_ID).withHost().build()
                .replace("{followingId}", userId1.toLongValue().toString())
                .replace("{followerId}", userId2.toLongValue().toString());
        mockMvc.perform(post(followUrl))
                .andExpectAll(
                        content().string(""),
                        status().isOk()

                );

        mockMvc.perform(get(postUrl2))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.subscribers[0].id").value(userId1.toLongValue())
                );
        mockMvc.perform(get(postUrl1))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.subscriptions[0].id").value(userId2.toLongValue())

                );

        log.info("======================Remove subscription======================");
        String unfollowUrl =
                new Fixtures.UrlBuilder(API_V1_USERS_UNFOLLOW_BY_ID).withHost().build()
                        .replace("{followingId}", userId1.toLongValue().toString())
                        .replace("{followerId}", userId2.toLongValue().toString());
        mockMvc.perform(post(unfollowUrl))
                .andExpectAll(
                        content().string(""),
                        status().isOk()
                );

        log.info("======================Check if user is unsubscribed======================");
        mockMvc.perform(get(postUrl2))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.subscribers").isEmpty()

                );
        mockMvc.perform(get(postUrl1))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.subscriptions").isEmpty()
                );

        log.info("======================Partially change user======================");

        var firstNameForUpdate = firstName();
        mockMvc.perform(put(API_V1_USERS_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        mapper.writeValueAsString(
                                new UpdateUserEndpoint.UpdateUserRestRequest(
                                        user1.id.toLongValue(),
                                        user1.userLogin.toStringValue(),
                                        firstNameForUpdate.toStringValue(),
                                        user1.patronymicName.toStringValue(),
                                        user1.lastName.toStringValue()
                                )
                        )
                )
        ).andExpectAll(
                jsonPath("$.id").value(user1.id.toLongValue()),
                jsonPath("$.login").value(user1.userLogin.toStringValue()),
                jsonPath("$.firstName").value(firstNameForUpdate.toStringValue()),
                jsonPath("$.patronymicName").value(user1.patronymicName.toStringValue()),
                jsonPath("$.lastName").value(user1.lastName.toStringValue()),
                status().isOk()
        );

        log.info("============================Delete users============================");

        mockMvc.perform(delete(postUrl1))
                .andExpect(content().string(""))
                .andExpect(status().isNoContent());
        mockMvc.perform(delete(postUrl2))
                .andExpect(content().string(""))
                .andExpect(status().isNoContent());

        log.info("============================Check if user is deleted============================");

        mockMvc.perform(get(postUrl1))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isNotFound(),
                        jsonPath("$.type").value(notFoundTypeUrl()),
                        jsonPath("$.status").value(HttpStatus.NOT_FOUND.value())
                );
        mockMvc.perform(get(postUrl2))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_PROBLEM_JSON),
                        status().isNotFound(),
                        jsonPath("$.type").value(notFoundTypeUrl()),
                        jsonPath("$.status").value(HttpStatus.NOT_FOUND.value())
                );

    }
}