package ru.skillbox.microservices.user.smoke;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.skillbox.microservices.user.application.configuration.new_gen.ApplicationNewGenConfiguration;
import ru.skillbox.microservices.user.domain.UserId;
import ru.skillbox.microservices.user.domain.UserLogin;
import ru.skillbox.microservices.user.domain.UserName;
import ru.skillbox.microservices.user.persistence.postgres.PostgresUserIdGenerator;
import ru.skillbox.microservices.user.persistence.postgres.TestConfiguration;
import ru.skillbox.microservices.user.rest.v1.AddUserEndpoint;
import ru.skillbox.microservices.user.rest.v1.testFixtures.Fixtures;

import javax.sql.DataSource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.skillbox.microservices.user.domain.testFixtures.Fixtures.*;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_ADD;
import static ru.skillbox.microservices.user.rest.v1.EndpointUrl.API_V1_USERS_GET_BY_ID;

@WebMvcTest
@ContextConfiguration(classes = {TestConfiguration.class, ApplicationNewGenConfiguration.class})
class SmokeTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    @SneakyThrows
    void createdSuccessfully() {

        var generator = new PostgresUserIdGenerator(dataSource);

        var userIdPrevious = generator.generate();
        var userId = UserId.fromLong(userIdPrevious.toLongValue() + 1).get();

        UserName firstName = firstName();
        UserName patronymicName = patronymicName();
        UserName lastName = lastName();
        UserLogin login = login(firstName.toStringValue(), lastName.toStringValue());

        var postUrl = new Fixtures.UrlBuilder(API_V1_USERS_ADD).withHost().build();

        mockMvc.perform(post(postUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        mapper.writeValueAsString(
                                new AddUserEndpoint.AddUserRestRequest(
                                        login.toStringValue(),
                                        firstName.toStringValue(),
                                        patronymicName.toStringValue(),
                                        lastName.toStringValue()
                                )
                        )
                )
        ).andExpectAll(
                content().string(""),
                status().isCreated(),
                header().string(
                        "Location",
                        (new Fixtures.UrlBuilder(API_V1_USERS_GET_BY_ID)
                                .withId(userId.toLongValue()).withHost().build()
                        )
                )
        );

        String getUrl = new Fixtures.UrlBuilder(API_V1_USERS_GET_BY_ID)
                .withId(userId.toLongValue()).withHost().build();
        mockMvc.perform(get(getUrl))
                .andExpectAll(
                        content().contentType(MediaType.APPLICATION_JSON),
                        jsonPath("$.id").value(userId.toLongValue()),
                        jsonPath("$.login").value(login.toStringValue()),
                        jsonPath("$.firstName").value(firstName.toStringValue()),
                        jsonPath("$.patronymicName").value(patronymicName.toStringValue()),
                        jsonPath("$.lastName").value(lastName.toStringValue())
                );

    }
}
