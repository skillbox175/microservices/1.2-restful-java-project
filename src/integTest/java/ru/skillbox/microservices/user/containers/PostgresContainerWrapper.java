package ru.skillbox.microservices.user.containers;

import org.testcontainers.containers.PostgreSQLContainer;

public class PostgresContainerWrapper extends PostgreSQLContainer<PostgresContainerWrapper> {
    private static final String POSTGRES_IMAGE_NAME = "postgres:15.1-alpine";
    private static final String POSTGRES_DB = "user-service-backend-db";
    private static final String POSTGRES_USER = "root";
    private static final String POSTGRES_PASSWORD = "123";

    public PostgresContainerWrapper() {
        super(POSTGRES_IMAGE_NAME);
        this
                // if you need container logger
                // .withLogConsumer(new Slf4jLogConsumer(log))
                .withDatabaseName(POSTGRES_DB)
                .withUsername(POSTGRES_USER)
                .withPassword(POSTGRES_PASSWORD);
//                .withCopyFileToContainer(MountableFile.forClasspathResource("source_on_host"),
//                        "destination_in_container");
    }

    @Override
    public void start() {
        super.start();
        this.getContainerId();
        // debug point. Container has to be already started
    }
}
