package ru.skillbox.microservices.user.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.skillbox.microservices.user.containers.PostgresContainerWrapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestMethodOrder(OrderAnnotation.class)
class UserControllerTest {

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;


    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }


    @BeforeEach
    private void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @Order(1)
    void createUser() throws Exception {
        String request = "{\"login\": \"vasya.petrov\", \"firstName\": \"Vasya\", \"lastName\": \"Petrov\", \"patronymicName\" : \"Ivanovich\"}";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions.andDo(print()).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo("Пользователь vasya.petrov добавлен в базу с id = 4")));
    }

    @Test
    @Order(2)
    void getUser() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/4")
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", Matchers.equalTo("Vasya")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", Matchers.equalTo("Petrov")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.deleted").value(Boolean.FALSE));
    }

    @Test
    @Order(3)
    void updateUser() throws Exception {
        String request = "{\"login\": \"vasya.ivanov\",\"firstName\": \"Vasya\",\"lastName\": \"Ivanov\", \"patronymicName\": \"Petrovich\"}";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put("/users/4")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo("Пользователь vasya.ivanov успешно сохранен")));
    }

    @Test
    @Order(4)
    void subscribeToUser() throws Exception {
        String request = "{\"id\": \"3\"}";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users/4/subscribe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo("Подписчик vanya.petrov успешно добавлен для пользователя vasya.petrov")));
    }

    @Test
    @Order(5)
    void getUserSubscriptionsAfterSubscribe() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/4").accept(MediaType.APPLICATION_JSON)).andReturn();

        String stringResult = mvcResult.getResponse().getContentAsString();
        String expectedResponse = "{\"id\":4,\"login\":\"vasya.petrov\",\"firstName\":\"Vasya\",\"lastName\":\"Petrov\",\"patronymicName\":\"Ivanovich\",\"subscribers\":[{\"id\":3,\"login\":\"vanya.petrov\",\"firstName\":\"Vanya\",\"lastName\":\"Petrov\",\"patronymicName\":\"Sidorovich\",\"deleted\":false}],\"subscriptions\":[],\"deleted\":false}";

        assertEquals(stringResult, expectedResponse);
    }

    @Test
    @Order(6)
    void unsubscribeFromUser() throws Exception {
        String request = "{\"id\": \"3\"}";
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users/4/unsubscribe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo("Подписчик vanya.petrov успешно удален для пользователя vasya.petrov"))
                );
    }

    @Test
    @Order(7)
    void getUserSubscriptionsAfterUnsubscribe() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/4").accept(MediaType.APPLICATION_JSON)).andReturn();

        String stringResult = mvcResult.getResponse().getContentAsString();
        String expectedResponse = "{\"id\":4,\"login\":\"vasya.petrov\",\"firstName\":\"Vasya\",\"lastName\":\"Petrov\",\"patronymicName\":\"Ivanovich\",\"subscribers\":[],\"subscriptions\":[],\"deleted\":false}";

        assertEquals(expectedResponse, stringResult);
    }

    @Test
    @Order(8)
    void deleteUser() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/4")
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo("Пользователь vasya.petrov успешно удален")));
    }

    @Test
    @Order(9)
    void getDeletedUser() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/4")
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is4xxClientError()
                );
    }

    @Test
    @Order(10)
    void findAll() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users")
        );
        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName", Matchers.equalTo("Vasya")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName", Matchers.equalTo("Ivanov")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].firstName", Matchers.equalTo("Petya")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].lastName", Matchers.equalTo("Sidorov")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].firstName", Matchers.equalTo("Vanya")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].lastName", Matchers.equalTo("Petrov")));
    }
}